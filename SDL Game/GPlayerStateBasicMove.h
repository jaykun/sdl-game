#pragma once
#ifndef GPLAYERSTATEBASICMOVE_H_
#define GPLAYERSTATEBASICMOVE_H_

#include "GState.h"

class GPlayerStateBasicMove : public GState
{
public:
	GPlayerStateBasicMove(GPlayer* player);// = default;
	virtual ~GPlayerStateBasicMove() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif