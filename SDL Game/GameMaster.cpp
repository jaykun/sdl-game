#include "stdafx.h"
#include "GameMaster.h"
#include "Define.h"
#include "GArea.h"
#include "GCamera.h"
#include "GPLayer.h"


#include <iostream>
#include <string>

GameMaster::GameMaster()
{
	window = nullptr;
	running = true;
}

std::string GameMaster::getBaseDir(const std::string &subDir)
{
#ifdef _WIN32
	const char PATH_SEP = '\\';
#else
	const char PATH_SEP = '/';
#endif
	std::string baseD(SDL_GetBasePath());
	return subDir.empty() ? baseD : baseD + subDir/* + PATH_SEP*/;
}

int GameMaster::OnExecute()
{
	if (OnInit() == false)
	{
		return -1;
	}

	SDL_Event event;

	while (running)
	{
		while (SDL_PollEvent(&event))
		{
			GEvent::OnEvent(&event);
		}

		//Testing MVC architecture with limited fps in loop
		CalculateDelta();
		OnInput();
		OnLoop();
		OnRender();
		

	}

	OnCleanup();

	return 0;
}

int main(int argc, char* argv[])
{
	GameMaster gm;
	
	return gm.OnExecute();
}