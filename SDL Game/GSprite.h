#pragma once
#ifndef GSPRITE_H_
#define GSPRITE_H_

#include "SDL.h"
#include <iostream>
#include <string>

class GSprite : public GTexture
{
public:

	//Extra information
	Uint32 frame_width = 0;
	Uint32 frame_height = 0;

	Uint32 col_type = 0;

	Uint32 col_x = 0;
	Uint32 col_y = 0;
	Uint32 col_width = 0;
	Uint32 col_height = 0;

	Uint32 max_frames = 0;


};

#endif