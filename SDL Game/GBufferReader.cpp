#include "stdafx.h"
#include "GBufferReader.h"

uint64_t GBufferReader::current_time;

GBufferReader::GBufferReader()
{
	//input = GInputPlayer::GetInput(0);
}

void GBufferReader::SetInput(GInput *input)
{
	this->input = input;
}

GInput* GBufferReader::GetInput()
{
	return input;
}

void GBufferReader::ReadInput()
{
	if (input == nullptr)
		return;

	//cache time from each input state here
	uint64_t time = 0;

	//these need to be reset each frame
	state_pressed = 0;
	state_released = 0;

	uint8_t buff_start = input->GetBufferStart();
	uint8_t buff_index = input->GetBufferIndex();
	//iterate buffer from start to latest, or until current time is reached
	for (uint8_t buff_iter = buff_start; time <= current_time && buff_iter != buff_index; buff_iter++)
	{
		//simple input
		prev_state = state;
		state = input->GetStateBuffer()[buff_iter];
		//XOR-operation, the bits that have been changed between states are now '1'
		uint32_t state_change = state ^ prev_state;

		//for use with advanced input
		//AND-operation, the bits that have been changed and are '1' in current state have been recently pressed
		uint32_t state_pressed_temp = state_change & state;
		//AND-operation, the bits that have been changed and are '0' in current state have been recently released
		uint32_t state_released_temp = state_change & (~state);

		//for use with simple input
		state_pressed |= state_pressed_temp;
		state_released |= state_released_temp;

		//for use with loop
		time = input->GetTimestampBuffer()[buff_iter];
	}
}

void GBufferReader::UpdateTime(uint64_t now)
{
	current_time = now;
}

uint32_t GBufferReader::GetState()
{
	return state;
}

uint32_t GBufferReader::GetStatePressed()
{
	return state_pressed;
}

uint32_t GBufferReader::GetStateReleased()
{
	return state_released;
}