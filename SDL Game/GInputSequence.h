#pragma once
#ifndef GINPUTSEQUENCE_H_
#define GINPUTSEQUENCE_H_

#include <cstdint>
#include <iostream>

class GInputSequence
{
public:
	 GInputSequence() = default;
	 ~GInputSequence() = default;
	 void SetTimelimit(uint64_t max_ms);
	 void SetSequence(uint32_t button);
	 bool Handle(uint32_t state, uint32_t state_pressed, uint32_t state_released, uint64_t time);
	 void Active(bool val);
	 bool Active();
private:
	uint64_t max_ms = 0;
	uint64_t max_time = 0;	
	uint32_t seqnum = 0;
	uint32_t final_cmd = UINT32_MAX;
	//10 should be enough for all input sequences
	uint32_t sequence[10];
	bool active = false;
};




#endif