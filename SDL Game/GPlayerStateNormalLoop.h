#pragma once
#ifndef GPLAYERSTATENORMALLOOP_H_
#define GPLAYERSTATENORMALLOOP_H_

#include "GState.h"

class GPlayerStateNormalLoop : public GState
{
public:
	GPlayerStateNormalLoop(GPlayer* player);// = default;
	virtual ~GPlayerStateNormalLoop() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif