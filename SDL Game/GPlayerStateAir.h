#pragma once
#ifndef GPLAYERSTATEAIR_H_
#define GPLAYERSTATEAIR_H_

#include "GState.h"

class GPlayerStateAir : public GState
{
public:
	GPlayerStateAir(GPlayer* player);// = default;
	virtual ~GPlayerStateAir() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif