#include "stdafx.h"
#include "GArea.h"
#include "HelperFunctions.h"

GArea GArea::AreaControl;

GArea::GArea() 
{
	AreaSize = 0;
}

//Load area from file
bool GArea::OnLoad(const char* File, SDL_Renderer* renderer, GTexture *tileSet) 
{
	MapList.clear();

	text = tileSet;

	//Attempt to open file
	FILE* FileHandle = nullptr;

	errno_t err = fopen_s(&FileHandle, File, "r");

	if (err != 0) 
	{
		std::cout << "Unable to open " << File << std::endl;
		return false;
	}

	//Attempt to retrieve tileset filename
	char TilesetFile[255];
	fscanf_s(FileHandle, "%254s\n", TilesetFile,255);
	
	if ((text->OnLoad(TilesetFile, renderer)) == false)
	{
		fclose(FileHandle);
		return false;
	}
	
	//Attempt to retrieve AreaSize
	fscanf_s(FileHandle, "%d\n", &AreaSize);

	//Attempt to retrieve Areasize*Areasize count of Mapfiles
	for (int X = 0; X < AreaSize; X++) 
	{
		for (int Y = 0; Y < AreaSize; Y++) 
		{
			char MapFile[255];
			fscanf_s(FileHandle, "%254s ", MapFile,255);	

			GMap tempMap;
			if (tempMap.OnLoad(getBaseDir(MapFile).c_str()) == false) 
			{
				fclose(FileHandle);
				return false;
			}

			tempMap.text = text;
			MapList.push_back(tempMap);
		}
		fscanf_s(FileHandle, "\n");
	}

	fclose(FileHandle);

	return true;
}

//hardware rendering
void GArea::OnRender(SDL_Renderer *renderer, int CameraX, int CameraY)
{
	if (!render)
		return;

	//Get width and height of a map in pixels
	int MapWidth = MAP_WIDTH * TILE_SIZE;
	int MapHeight = MAP_HEIGHT * TILE_SIZE;

	//Find map which is found in Camera's top left corner
	int FirstID = CameraX / MapWidth;
	FirstID = FirstID + ((CameraY / MapHeight) * AreaSize);

	//Math magic to determine the 4 maps inside the camera's borders and render them to screen
	for (int i = 0; i < 4; i++)
	{
		int ID = FirstID + ((i / 2) * AreaSize) + (i % 2);

		if (ID < 0 || ID >= MapList.size()) continue;

		int X = ((ID % AreaSize) * MapWidth) - CameraX;
		int Y = ((ID / AreaSize) * MapHeight) - CameraY;

		MapList[ID].OnRender(renderer, X, Y);
	}
}

void GArea::OnCleanup() 
{
	if (text)
	{
		SDL_DestroyTexture(text->GetTexture());
	}

	MapList.clear();
}

GMap* GArea::GetMap(int X, int Y) 
{
	int MapWidth = MAP_WIDTH * TILE_SIZE;
	int MapHeight = MAP_HEIGHT * TILE_SIZE;

	int ID = X / MapWidth;
	ID = ID + ((Y / MapHeight) * AreaSize);

	if (ID < 0 || ID >= MapList.size()) 
	{
		return NULL;
	}

	return &MapList[ID];
}

GTile* GArea::GetTile(int X, int Y) 
{
	int MapWidth = MAP_WIDTH * TILE_SIZE;
	int MapHeight = MAP_HEIGHT * TILE_SIZE;

	GMap* Map = GetMap(X, Y);

	if (Map == NULL) return NULL;

	X = X % MapWidth;
	Y = Y % MapHeight;

	return Map->GetTile(X, Y);
}