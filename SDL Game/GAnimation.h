#pragma once
#ifndef GANIMATION_H_
#define GANIMATION_H_

#include <SDL.h>

class GAnimation 
{
public:
	GAnimation();
	GAnimation(float CurrentFrame_, float MaxFrames_, float FrameRate, bool Oscillate);

	void OnAnimate(float delta_time, float timeFactor_ = 1);
	void SetFrameRate(float Rate);

	void SetCurrentFrame(float Frame);

	int GetCurrentFrame();

	int MaxFrames = 0;
	bool Oscillate = false;

	int CurrentFrameCol;
	int CurrentFrameRow;

private:
	float CurrentFrame = 0;
	float FrameRate = 0.100;
	float FrameInc = 0.100;
};

#endif
