#pragma once
#ifndef GPLAYERSTATESPECIALMOVE1_H_
#define GPLAYERSTATESPECIALMOVE1_H_

#include "GState.h"

class GPlayerStateSpecialMove1 : public GState
{
public:
	GPlayerStateSpecialMove1(GPlayer* player);// = default;
	virtual ~GPlayerStateSpecialMove1() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:
	int dir;
	int dest_x;

private:


};

#endif