#pragma once
#ifndef GPLAYERSTATEJUMPAIRRUN_H_
#define GPLAYERSTATEJUMPAIRRUN_H_


#include "GState.h"

class GPlayerStateJumpAirRun : public GState
{
public:
	GPlayerStateJumpAirRun(GPlayer* player);// = default;
	virtual ~GPlayerStateJumpAirRun() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif