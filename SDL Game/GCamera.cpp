#include "stdafx.h"
#include "GCamera.h"
#include "HelperFunctions.h"

GCamera GCamera::CameraControl;

GCamera::GCamera() 
{
	x = y = 0;

	//target_object_x = target_object_y = nullptr;

	TargetMode = TARGET_MODE_NORMAL;
}

void GCamera::OnInit(GPlayer &player_)
{
	player = &player_;
}

void GCamera::OnMove(int MoveX, int MoveY) 
{
	x += MoveX;
	y += MoveY;
}

void GCamera::OnLoop(float delta_time, float extrapolation, float time_factor)
{
	if (!loop)
		return;

	//SHAKE
	//should probably be handled in another function, inside OnLoop()
	switch (shake_seq)
	{
		//Camera will be offset when shake_seq is 1
	case 1:
		//randomize shake amount according to given number, clamp it so shaking doesn't shake the screen outside room
		xshake = clamp(x + random_int(-xshake_amount, xshake_amount), 0, 1920 - WWIDTH) - x;
		yshake = clamp(y + random_int(-yshake_amount, yshake_amount), 0, 1440 - WHEIGHT) - y;

		shake_seq = 2;

		break;
	case 2:
		//this neutralizes any future offset done in SHAKE PT2 at the end of this function
		x -= xshake;
		y -= yshake;

		//Once shake_threshold is greater than 1, we will return to original camera coordinates
		//Shaking will continue until shakecount is 0
		shake_threshold += delta_time;

		if (shake_threshold >= 1)
		{
			//reset shake_threshold for the next round of shaking
			shake_threshold = 0;

			//reset shakes back to 0. When combined with the operation SHAKE PT2 below, the 'shake' offset is removed and the view is visibly back at it's current position
			xshake = 0;
			yshake = 0;

			//now we have done a single shake of the view
			shakecount -= 1;

			//if shakecount is > 0, go to sequence 1, else go to sequence default
			shake_seq = signOf(shakecount);
		}

		break;
	default:

		break;
	}

	//HORIZONTAL
	//Follows player with player at 1/3 of the width of screen

	//determine which side of the screen is the "center" and it's coordinate
	center_x = x + middle_x - cam_dir_x * center_offset_x;

	//for extrapolation, other modes may need tweaking
	int dist_from_center_x = player->GetRenderX(extrapolation) + player->texture->frame_width / 2 - center_x;

	//if player distance from the center of camera rectangle is greater than the borders of said rectangle
	//in other words, is the player within the camera rectangle
	if (abs(dist_from_center_x) > border_x)
	{
		frames_processed_x++;

		//what dir does the camera need to move to catch player
		cam_dir_x = signOf(dist_from_center_x);

		//target position is current location of camera plus player distance from camerarect center, minus the border
		//in other words, the point where center of player touches camerarect border
		target_pos_x = clamp(x + dist_from_center_x - cam_dir_x * border_x, 0, 1920 - WWIDTH);

		//start_pos_x is value of camera x before movement was required
		//target_pos_x is updated each frame depending on player location
		float distance_to_be_moved_x = abs(start_pos_x - target_pos_x);
		

		/*if (5.0f > distance_to_be_moved_x / frames_to_target)
		{
			std::cout << "speed" << std::endl;
		}
		else
		{
			std::cout << "percent" << std::endl;
		}*/

		//use minimum x speed of cam_speed_x, otherwise ceomplete movement in "frames_to_target" frames
		//use delta_time only in minimum speed, as dist/frames is already according to dt (1s = 60* dt of 1)
		float step = std::max(cam_speed_x * delta_time, (distance_to_be_moved_x / frames_to_target_x));
		distance_moved_x += step;
		float distance_moved_x_temp = clamp(std::max(distance_moved_x, distance_to_be_moved_x / frames_to_target_x * frames_processed_x), 0.0f, distance_to_be_moved_x);

		//if camera movement needs to be completed always in the same time, no minimum speed
		/*float step = distance_to_be_moved_x / frames_to_target;
		distance_moved_x += step;
		float distance_moved_x_temp = clamp(distance_moved_x, 0.0f, distance_to_be_moved_x);*/

		
		//this creates a rubberband effect, not good
		//use x = lerp(start_pos_x, target_pos_x, percent / distance_to_be_moved_x); with this
		//float percent = frames_processed / frames_to_target;

		//for testing
		int prev_x;

		if (start_pos_x != target_pos_x)
		{
			//for testing
			prev_x = x;

			//x is between start_pos and end_pos, determined by the percentage value of (distance_moved_x divided by distance_to_be_moved_x)

			//This is no longer 60 frames, but the speed has acceleration and deacceleration which looks cool. Around 78 frames
			float func_percentage = clamp(bias(function_value_x, distance_moved_x / distance_to_be_moved_x), 0.0f, 1.0f);
			x = lerp(start_pos_x, target_pos_x, func_percentage);

			//60 frames version
			//float percentage = clamp( distance_moved_x / distance_to_be_moved_x, 0.0f, 1.0f);
			//x = lerp(start_pos_x, target_pos_x, percentage);

			frames_used++;
			time_acc_counter += delta_time;
		}

		//sometimes this if is not true, but the next frame has player inside the border
		//this is where the > 60 movement frames must come from
		if (x == target_pos_x)
		{
			if (frames_used > 5)
				std::cout << "frames used: " << frames_used << std::endl;
				/*std::cout << "end" << std::endl;
				std::cout << std::endl;*/
			frames_used = 0;
			time_acc_counter = 0;
		}

		camera_moved_x = true;
	}
	else
	{
		start_pos_x = x;
		distance_moved_x = 0;
		frames_processed_x = 0.0f;

		//if (camera_moved_x)
		//{
		//	if (frames_used > 5)
		//		std::cout << "frames used: " << frames_used << std::endl;
		//	/*std::cout << "end" << std::endl;
		//	std::cout << std::endl;*/
		//	frames_used = 0;
		//	time_acc_counter = 0;
		//	camera_moved_x = false;
		//}

		frames_used = 0;

	}


	


	

	if (player->GetGrounded())
	{
		border_y = 64;
		center_y = y + middle_y + border_y;
		border_y = 0;
	}
	else
	{
		border_y = 128;
		center_y = y + middle_y;
	}

	int dist_from_center_y = player->GetRenderY(extrapolation) + player->height - center_y;

	if (abs(dist_from_center_y) > border_y)
	{
		frames_processed_y++;

		cam_dir_y = signOf(dist_from_center_y);

		target_pos_y = clamp(y + dist_from_center_y - cam_dir_y * border_y, 0, 1920 - WHEIGHT);

		float distance_to_be_moved_y = abs(start_pos_y - target_pos_y);

		//there's some controversy to this, look horizontal section for more info
		float step = std::max(cam_speed_y * delta_time, (distance_to_be_moved_y / frames_to_target_y));
		distance_moved_y += step;
		float distance_moved_y_temp = clamp(std::max(distance_moved_y, distance_to_be_moved_y / frames_to_target_y * frames_processed_y), 0.0f, distance_to_be_moved_y);

		if (start_pos_y != target_pos_y)
		{
			float func_percentage = clamp(bias(function_value_y, distance_moved_y / distance_to_be_moved_y), 0.0f, 1.0f);
			y = lerp(start_pos_y, target_pos_y, bias(function_value_y, func_percentage));

			//non-biased version, 60frames
			/*float percentage = clamp(distance_moved_y / distance_to_be_moved_y, 0.0f, 1.0f);
			y = lerp(start_pos_y, target_pos_y, bias(function_value_y, percentage));*/
		}
	}
	else
	{
		start_pos_y = y;
		distance_moved_y = 0;
		frames_processed_y = 0.0f;
	}





	//VERTICAL
	//Snaps to platform below when grounded
	/*center_y = y + middle_y;

	if (player->GetGrounded())
	{
		border_y = 64;
	}
	else
	{
		border_y = 128;
	}*/


	/*if (abs(dist_from_center_y) > border_y)
	{
		cam_dir_y = signOf(dist_from_center_y);

		cam_speed_y = 4 * time_acc;

		target_pos_y = clamp(y + dist_from_center_y - cam_dir_y * border_y, 0, 1440 - WHEIGHT);

		float distance_to_be_moved_y = abs(start_pos_y - target_pos_y);

		distance_moved_y = clamp(distance_moved_y + cam_speed_y, 0.0f, distance_to_be_moved_y);

		if (start_pos_y != target_pos_y)
		{
			y = lerp(start_pos_y, target_pos_y, bias(function_value_y, distance_moved_y / distance_to_be_moved_y));
		}
	}
	else
	{
		start_pos_y = y;
		distance_moved_y = 0;
	}*/

	//SHAKE PT2
	//Move to onloop somehow, or maybe this should stay in render afterall
	x += xshake;
	y += yshake;

}

//Works fine but not perfect, keep this here for posterity
void GCamera::OnLoop2(float delta_real, float delta_time)
{
	if (!loop)
		return;	

	//SHAKE
	//should probably be handled in another function, inside OnLoop()
	switch (shake_seq)
	{
		//Camera will be offset when shake_seq is 1
	case 1:
		//randomize shake amount according to given number, clamp it so shaking doesn't shake the screen outside room
		xshake = clamp(x + random_int(-xshake_amount, xshake_amount), 0, 1920 - WWIDTH) - x;
		yshake = clamp(y + random_int(-yshake_amount, yshake_amount), 0, 1440 - WHEIGHT) - y;

		shake_seq = 2;

		break;
	case 2:
		//this neutralizes any future offset done in SHAKE PT2 at the end of this function
		x -= xshake;
		y -= yshake;

		//Once shake_threshold is greater than 1, we will return to original camera coordinates
		//Shaking will continue until shakecount is 0
		shake_threshold += delta_time;

		if (shake_threshold >= 1)
		{
			//reset shake_threshold for the next round of shaking
			shake_threshold = 0;

			//reset shakes back to 0. When combined with the operation SHAKE PT2 below, the 'shake' offset is removed and the view is visibly back at it's current position
			xshake = 0;
			yshake = 0;

			//now we have done a single shake of the view
			shakecount -= 1;

			//if shakecount is > 0, go to sequence 1, else go to sequence default
			shake_seq = signOf(shakecount);
		}

		break;
	default:

		break;
	}

	//HORIZONTAL
	//Follows player with player at 1/3 of the width of screen

	//determine which side of the screen is the "center" and it's coordinate
	center_x = x + middle_x - cam_dir_x * center_offset_x;

	//for extrapolation, other modes may need tweaking
	int dist_from_center_x = player->GetRenderX(delta_real) + player->texture->frame_width / 2 - center_x;

	//if player distance from the center of camera rectangle is greater than the borders of said rectangle
	//in other words, is the player within the camera rectangle
	if (abs(dist_from_center_x) > border_x)
	{
		//what dir does the camera need to move to catch player
		cam_dir_x = signOf(dist_from_center_x);

		//used to track player speed
		float prev_target_pos_x = target_pos_x;

		//target position is current location of camera plus player distance from camerarect center, minus the border
		//in other words, the point where center of player touches camerarect border
		target_pos_x = clamp(x + dist_from_center_x - cam_dir_x * border_x, 0, 1920 - WWIDTH);

		//for whatever reason, speed of 1.0 works for release, 5.0 for debug. but why?!?!
		//somewhat consistent
		cam_speed_x = clamp(5.0 + abs(target_pos_x - prev_target_pos_x), 5.0, 50.0) * delta_time;
		//too fast and inconsistent, perhaps we need to forecast player speed better
		//cam_speed_x = clamp(5.0 + abs(player->GetSpeedX()), 5.0, 50.0) * time_acc;
		//testing
		//cam_speed_x = clamp(5.0 + abs(player->GetRenderX(1.0) - player->x + player->fraction_x), 5.0, 50.0) * time_acc;

		//start_pos_x is value of camera x before movement was required
		//target_pos_x is updated each frame depending on player location
		float distance_to_be_moved_x = abs(start_pos_x - target_pos_x);

		//distance_moved_x starts at 0, cam_speed_x gets added to it each frame until it reaches distance_to_be_travelled_x
		distance_moved_x = clamp(distance_moved_x + cam_speed_x, 0.0f, distance_to_be_moved_x);

		//for testing
		int prev_x;

		if (start_pos_x != target_pos_x)
		{
			//for testing
			prev_x = x;

			//x is between start_pos and end_pos, determined by the percentage value of (distance_moved_x divided by distance_to_be_moved_x)
			//x = lerp(start_pos_x, target_pos_x, bias(function_value_x, distance_moved_x / distance_to_be_moved_x));
			x = lerp(start_pos_x, target_pos_x, distance_moved_x / distance_to_be_moved_x);

			//for testing
			if (x - prev_x != 0)
			{
				/*std::cout << "cam_speed_x = " << clamp(5.0 + abs(player->GetSpeedX()), 0.0, 20.0) << ", player speed_x " << abs(player->GetSpeedX()) << std::endl;
				std::cout << "cam_speed_x = " << cam_speed_x << ", player speed_x " << abs(player->GetSpeedX()) * delta_real << std::endl;
				std::cout << std::endl;*/

				//std::cout << "relative speed = " << abs(cam_speed_x) - abs(player->GetSpeedX())  << std::endl;
				//std::cout << std::endl;
			}
			frames_used++;
			time_acc_counter += delta_time;
		}

		//std::cout << player->x << " vs " << player->GetRenderX(frac(delta_real)) << " = " << player->x - player->GetRenderX(frac(delta_real)) << std::endl;
		//std::cout << distance_moved_x / distance_to_be_moved_x << std::endl;

		if (x == target_pos_x)
		{
			/*if (frames_used > 5)
				std::cout << "frames used: " << time_acc_counter << std::endl;*/
			/*std::cout << "end" << std::endl;
			std::cout << std::endl;*/
			frames_used = 0;
			time_acc_counter = 0;
		}

		//alternative, but gain/bias cannot be used here
		/*if (x != target_pos_x)
		{
			if (x > target_pos_x)
			{
				x = clamp(x-cam_speed_x, target_pos_x, (float)(1920 - WWIDTH));
			}
			else
			{
				x = clamp(x+cam_speed_x, 0.0, target_pos_x);
			}
		}*/


	}
	else
	{
		start_pos_x = x;
		distance_moved_x = 0;		
	}


	//VERTICAL
	//Snaps to platform below when grounded
	center_y = y + middle_y;

	if (player->GetGrounded())
	{
		border_y = 64;
	}
	else
	{
		border_y = 128;
	}


	int dist_from_center_y = player->GetRenderY(delta_real) - center_y;
	

	if (abs(dist_from_center_y) > border_y)
	{
		cam_dir_y = signOf(dist_from_center_y);

		cam_speed_y = 4 * delta_time;

		target_pos_y = clamp(y + dist_from_center_y - cam_dir_y * border_y, 0, 1440 - WHEIGHT);

		float distance_to_be_moved_y = abs(start_pos_y - target_pos_y);

		distance_moved_y = clamp(distance_moved_y + cam_speed_y, 0.0f, distance_to_be_moved_y);

		if (start_pos_y != target_pos_y)
		{
			y = lerp(start_pos_y, target_pos_y, bias(function_value_y, distance_moved_y / distance_to_be_moved_y));
		}
	}
	else
	{
		start_pos_y = y;
		distance_moved_y = 0;
	}

	//SHAKE PT2
	//Move to onloop somehow, or maybe this should stay in render afterall
	x += xshake;
	y += yshake;

}

//Called from other entities, sets up shake
void GCamera::Shake(float delta_real, int x_, int y_, int count)
{
	shake_seq = 1;

	//So shake lasts the same time on all possible FPS
	if (delta_real > 1)
	{
		shakecount = count / delta_real;
	}
	else
	{
		shakecount = count;
	}

	xshake_amount = x_;
	yshake_amount = y_;
	
}

//Mostly for debug reasons, not currently necessary
void GCamera::OnRender(SDL_Renderer *renderer)
{
	if (!render)
		return;

	SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255); // the rect color (solid red)

	SDL_Rect rect;

	rect.x = center_x - border_x - x;
	rect.y = center_y - border_y - y;
	rect.w = border_x + border_x;
	rect.h = border_y + border_y;
	
	SDL_RenderDrawRect(renderer, &rect);
}

int GCamera::GetX() 
{
	return x;
}

int GCamera::GetY() 
{
	return y;
}

//Set camera position instantly
void GCamera::SetPos(int x_, int y_) 
{
	this->x = x_;
	this->y = y_;
}

//Camera needs more work before this is useful again
void GCamera::SetTarget(int& x_, int& y_) 
{
	/**target_object_x = x_;
	*target_object_y = y_;*/
}