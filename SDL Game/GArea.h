#pragma once
#ifndef CAREA_H_
#define CAREA_H_

#include "GMap.h"
#include "GTexture.h"


class GArea {
public:
	GArea();

	bool OnLoad(const char* File, SDL_Renderer* renderer, GTexture *tileSet);

	void OnRender(SDL_Renderer *renderer, int CameraX, int CameraY);

	void OnCleanup();

	GMap*    GetMap(int X, int Y);

	GTile*    GetTile(int X, int Y);

	std::vector<GMap> MapList;
	static GArea AreaControl;

	bool render = true;
private:
	int AreaSize;

	GTexture *text;
};

#endif
