#include "stdafx.h"
#include "GAnimation.h"
#include <iostream>
#include <string>

GAnimation::GAnimation()
{

}

GAnimation::GAnimation(float CurrentFrame_, float MaxFrames_, float FrameRate_, bool Oscillate_)
	: CurrentFrame(CurrentFrame_), MaxFrames(MaxFrames_), FrameRate(FrameRate_), FrameInc(FrameRate_), Oscillate(Oscillate_)
{

}

void GAnimation::OnAnimate(float delta_time, float timeFactor_)
{
	if (FrameRate == 0 || MaxFrames == 0) return;

	CurrentFrame += FrameInc * timeFactor_ * delta_time;

	if (CurrentFrame >= MaxFrames)
	{
		if (Oscillate)
		{
			FrameInc = -FrameRate;
		}
		else
		{
			CurrentFrame -= MaxFrames;
		}
	}
	else if (CurrentFrame <= 0)
	{
		if (Oscillate)
		{
			FrameInc = FrameRate;
		}
		else
		{
			CurrentFrame += MaxFrames;
		}
	}	
}

//Set animation speed
void GAnimation::SetFrameRate(float Rate)
{
	//Keep FrameInc sign as it was
	FrameInc != FrameRate ? FrameInc = -Rate : FrameInc = Rate;
	FrameRate = Rate;
}

void GAnimation::SetCurrentFrame(float Frame)
{
	if (Frame < 0 || Frame >= MaxFrames) return;

	CurrentFrame = Frame;
}

int GAnimation::GetCurrentFrame()
{
	return CurrentFrame;
}