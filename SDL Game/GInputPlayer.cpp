#include "stdafx.h"
#include "GInputPlayer.h"

GInputPlayer::GInputPlayer()
{
	if (count < MAX_INPUTS)
	{
		input[count] = this;
		count++;
	}
}


GInputPlayer::GInputPlayer(SDL_GameController *gP)
{
	SetGamepad(gP);

	_ASSERT(count < MAX_INPUTS);

	if (count < MAX_INPUTS)
	{
		input[count] = this;
		count++;
	}
}

GInputPlayer::~GInputPlayer()
{
	OnCleanUp();
}

//uint32_t *GInputPlayer::GetStateBuffer()
//{
//	return state;
//}
//
//uint64_t *GInputPlayer::GetTimestampBuffer()
//{
//	return timestamp;
//}
//
//uint8_t GInputPlayer::GetBufferStart()
//{
//	return buffer_start;
//}
//
//uint8_t GInputPlayer::GetBufferIndex()
//{
//	return buffer_index;
//}

GInputPlayer *GInputPlayer::GetInput(int number)
{
	return input[number];
}

int GInputPlayer::count = 0;
GInputPlayer *GInputPlayer::input[MAX_INPUTS]{ nullptr, nullptr, nullptr, nullptr };
//Static variable for KB status
const Uint8 *GInputPlayer::keystate = nullptr;

//Update KB for all inputs
void GInputPlayer::UpdateKB()
{
	keystate = SDL_GetKeyboardState(NULL);
}

void GInputPlayer::OnInput(uint64_t current_time, float delta_time)
{
	uint32_t kbstate = 0;
	//make sure that different GInputs don't share any KB keys, and that KB is always enabled at least in one GInput
	if (kBController)
	{
		kbstate += keystate[KBUP] << BUTTON_UP;
		kbstate += keystate[KBDOWN] << BUTTON_DOWN;
		kbstate += keystate[KBLEFT] << BUTTON_LEFT;
		kbstate += keystate[KBRIGHT] << BUTTON_RIGHT;
		kbstate += keystate[KBJUMP] << BUTTON_A;
		kbstate += keystate[KBRUN] << BUTTON_RT;
		kbstate += keystate[KBSTART] << BUTTON_START;
	}

	/*	to customize used buttons one could do the following:
		1) Create variables like so:
			GameControllerButton jump_button = CONTROLLER_BUTTON_A (or any other button
		2) Use these new variables below instead of CONTROLLER_BUTTON_A
	*/

	uint32_t gpstate = 0;
	if (gPController != nullptr)
	{
		gpstate += (GetGamePadButton(CONTROLLER_BUTTON_DPAD_UP) | GetGamePadButton(CONTROLLER_AXIS_LEFTY_UP, JOYSTICK_DEADZONE)) << BUTTON_UP;
		gpstate += (GetGamePadButton(CONTROLLER_BUTTON_DPAD_DOWN) | GetGamePadButton(CONTROLLER_AXIS_LEFTY_DOWN, JOYSTICK_DEADZONE)) << BUTTON_DOWN;
		gpstate += (GetGamePadButton(CONTROLLER_BUTTON_DPAD_LEFT) | GetGamePadButton(CONTROLLER_AXIS_LEFTX_LEFT, JOYSTICK_DEADZONE)) << BUTTON_LEFT;
		gpstate += (GetGamePadButton(CONTROLLER_BUTTON_DPAD_LEFT) | GetGamePadButton(CONTROLLER_AXIS_LEFTX_RIGHT, JOYSTICK_DEADZONE)) << BUTTON_RIGHT;
		gpstate += GetGamePadButton(CONTROLLER_BUTTON_A) << BUTTON_A;
		gpstate += GetGamePadButton(CONTROLLER_AXIS_TRIGGERRIGHT, JOYSTICK_DEADZONE) << BUTTON_RT;
		gpstate += GetGamePadButton(CONTROLLER_BUTTON_START) << BUTTON_START;


		/*gpstate += (SDL_GameControllerGetButton(gPController, SDL_CONTROLLER_BUTTON_DPAD_UP) | SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_LEFTY) < -JOYSTICK_DEADZONE) << BUTTON_UP;
		gpstate += (SDL_GameControllerGetButton(gPController, SDL_CONTROLLER_BUTTON_DPAD_DOWN) | SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_LEFTY) < JOYSTICK_DEADZONE) << BUTTON_DOWN;
		gpstate += (SDL_GameControllerGetButton(gPController, SDL_CONTROLLER_BUTTON_DPAD_LEFT) | SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_LEFTX) < -JOYSTICK_DEADZONE) << BUTTON_LEFT;
		gpstate += (SDL_GameControllerGetButton(gPController, SDL_CONTROLLER_BUTTON_DPAD_RIGHT) | SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_LEFTX) > JOYSTICK_DEADZONE) << BUTTON_RIGHT;
		gpstate += SDL_GameControllerGetButton(gPController, SDL_CONTROLLER_BUTTON_A) << BUTTON_A;
		gpstate += (SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_TRIGGERRIGHT) > JOYSTICK_DEADZONE) << BUTTON_RT;
		gpstate += SDL_GameControllerGetButton(gPController, SDL_CONTROLLER_BUTTON_START) << BUTTON_START;*/
	}

	uint32_t button_state = (kbstate | gpstate); 

	//this is a ring buffer https://gameprogrammingpatterns.com/event-queue.html
	//it uses uint8 overflow as a mechanism
	//if state differs from previous state, and there's still room left in the buffer, add new state to buffer
	//If buffer is filled, no more inputs are accepted until room is freed
	Uint8 lim = buffer_start - 1;
	if (button_state != prev_button_state && buffer_index != lim)
	{
		//add state to buffer
		state[buffer_index] = button_state;
		//add timestamp
		timestamp[buffer_index] = current_time;

		//increment index, so we don't overwrite the same location continuously in array
		buffer_index++;

		//save this button state so we can compare if the next state differs from current
		prev_button_state = button_state;
	}
}

//void GInputPlayer::FlushBufferBefore(uint64_t time)
//{
//	//this is a ring buffer https://gameprogrammingpatterns.com/event-queue.html
//	//it uses uint8 overflow as a mechanism
//	//increment buffer_start as long as timestamp is smaller than time or we reach the end of array
//	for (; timestamp[buffer_start] <= time && buffer_start != buffer_index; buffer_start++);
//}

uint8_t GInputPlayer::GetGamePadButton(GameControllerButton button, int deadzone)
{
	switch (button)
	{	
	case CONTROLLER_BUTTON_A:
	case CONTROLLER_BUTTON_B:
	case CONTROLLER_BUTTON_X:
	case CONTROLLER_BUTTON_Y:
	case CONTROLLER_BUTTON_BACK:
	case CONTROLLER_BUTTON_GUIDE:
	case CONTROLLER_BUTTON_START:
	case CONTROLLER_BUTTON_LEFTSTICK:
	case CONTROLLER_BUTTON_RIGHTSTICK:
	case CONTROLLER_BUTTON_LEFTSHOULDER:
	case CONTROLLER_BUTTON_RIGHTSHOULDER:
	case CONTROLLER_BUTTON_DPAD_UP:
	case CONTROLLER_BUTTON_DPAD_DOWN:
	case CONTROLLER_BUTTON_DPAD_LEFT:
	case CONTROLLER_BUTTON_DPAD_RIGHT:
		return SDL_GameControllerGetButton(gPController, SDL_GameControllerButton(button));

	case CONTROLLER_AXIS_TRIGGERLEFT:
		return SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_TRIGGERLEFT) > deadzone;
	case CONTROLLER_AXIS_TRIGGERRIGHT:
		return SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_TRIGGERRIGHT) > deadzone;

	case CONTROLLER_AXIS_LEFTX_LEFT:
			return SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_LEFTX) < -deadzone;
	case CONTROLLER_AXIS_LEFTX_RIGHT:
			return SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_LEFTX) > deadzone;
	case CONTROLLER_AXIS_LEFTY_UP:
			return SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_LEFTY) < -deadzone;
	case CONTROLLER_AXIS_LEFTY_DOWN:
			return SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_LEFTY) > deadzone;
	case CONTROLLER_AXIS_RIGHTX_LEFT:
			return SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_RIGHTX) < -deadzone;
	case CONTROLLER_AXIS_RIGHTX_RIGHT:
			return SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_RIGHTX) > deadzone;
	case CONTROLLER_AXIS_RIGHTY_UP:
			return SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_RIGHTY) < -deadzone;
	case CONTROLLER_AXIS_RIGHTY_DOWN:
			return SDL_GameControllerGetAxis(gPController, SDL_CONTROLLER_AXIS_RIGHTY) > deadzone;
	default :
		return 0;
	}
}

void  GInputPlayer::SetGamepad(SDL_GameController *gP)
{
	if (hapticDevice != nullptr)
	{
		SDL_HapticClose(hapticDevice);
	}

	gPController = gP;

	SetHaptic();
}

SDL_GameController *GInputPlayer::GetGamePad()
{
	return gPController;
}

void  GInputPlayer::SetKeyboard(bool kb)
{
	kBController = kb;
}

void  GInputPlayer::SetHaptic()
{
	if (gPController == nullptr) return;

	SDL_Joystick *JoystickHandle = SDL_GameControllerGetJoystick(gPController);
	hapticDevice = SDL_HapticOpenFromJoystick(JoystickHandle);

	if (SDL_HapticRumbleInit(hapticDevice) != 0)
	{
		SDL_HapticClose(hapticDevice);
	}
}

void GInputPlayer::RemoveHaptic()
{
	if (hapticDevice != nullptr)
	{
		SDL_HapticClose(hapticDevice);
		hapticDevice = nullptr;
	}
}

void GInputPlayer::Rumble(float strength, int length)
{
	if (!rumble) return;

	if (hapticDevice)
	{
		SDL_HapticRumblePlay(hapticDevice, strength, length);
	}
}

void GInputPlayer::OnCleanUp()
{
	SDL_HapticClose(hapticDevice);
}