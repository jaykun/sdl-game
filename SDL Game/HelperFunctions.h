#pragma once
#ifndef HELPERFUNCTIONS_H_
#define HELPERFUNCTIONS_H_

#include <iostream>
#include <string>
#include <algorithm>
#include <SDL.h>



std::string getBaseDir(const std::string &subDir = "");

//basic random integer function
int random_int(const int &min, const int &max);


//Get sign, -1,0 or 1
template <typename T>
inline int signOf(T x)
{
	return (x > 0) - (x < 0);
}

//Linear interpolate
template <typename T, typename U>
float lerp(T a, T b, U f)
{
	return (1 - f)*a + f*b;
}

//Clamp variable to min and max values
template <typename T>
T clamp(const T& n, const T& lower, const T& upper) 
{
	return std::max(lower, std::min(n, upper));
}

//Gain, useful with lerp
template <typename T>
T gain(const T& a, const T& b)
{
	T p = (1 / a - 2) * (1 - 2 * b);

	if (b < 0.5)
	{
		return b / (p + 1);
	}
	else
	{
		return (p - b) / (p - 1);
	}
}

//Bias, useful with lerp
template <typename T>
T bias(const T& a, const T& b) 
{
	return  b / ((1 / a - 2) * (1 - b) + 1);
}

//Get fractional part of a floating point variable
template <typename T>
T frac(const T& a)
{
	return a - ((int64_t)a);
}


#endif
