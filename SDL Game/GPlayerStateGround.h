#pragma once
#ifndef GPLAYERSTATEGROUND_H_
#define GPLAYERSTATEGROUND_H_


#include "GState.h"

class GPlayerStateGround : public GState
{
public:
	GPlayerStateGround(GPlayer* player);// = default;
	virtual ~GPlayerStateGround() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif