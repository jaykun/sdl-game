#include "stdafx.h"
#include "GSprite.h"
#include "GFPS.h"

#include <iostream>
#include <string>
#include "SDL_image.h"

GSprite::GSprite()
{

}


bool GSprite::OnLoad(const std::string fileName, int Width_, int Height_, int Col_X_, int Col_Y_, int Col_Width_, int Col_Height_, int MaxFrames_)
{
	std::string fileLocation(SDL_GetBasePath());
	fileLocation += fileName;

	if ((this->image = IMG_Load(fileLocation.c_str())) == nullptr)
	{
		return false;
	}

	GSprite::Transparent(255, 0, 255);

	this->Width = Width_;
	this->Height = Height_;

	this->Col_X = Col_X_;
	this->Col_Y = Col_Y_;
	this->Col_Width = Col_Width_;
	this->Col_Height = Col_Height_;

	this->MaxFrames = MaxFrames_;

	return true;
}


//Draw whole image to a surface
bool GSprite::OnDraw(SDL_Surface *surfDest, int x, int y)
{
	if (surfDest == nullptr || image == nullptr)
	{
		return false;
	}

	SDL_Rect destR;
	destR.x = x;
	destR.y = y;

	SDL_BlitSurface(image, NULL, surfDest, &destR);

	return true;
}

//Draw part of image
bool GSprite::OnDraw(SDL_Surface *surfDest,  int x, int y, int x2, int y2, int w, int h)
{
	if (surfDest == nullptr || image == nullptr)
	{
		return false;
	}

	SDL_Rect destR;

	destR.x = x;
	destR.y = y;

	SDL_Rect srcR;

	srcR.x = x2;
	srcR.y = y2;
	srcR.w = w;
	srcR.h = h;

	SDL_BlitSurface(image, &srcR, surfDest, &destR);

	return true;
}

//Set transparency according to colour
bool GSprite::Transparent(int r, int g, int b)
{
	if (this->image == NULL)
	{
		return false;
	}

	SDL_SetColorKey(this->image, SDL_TRUE, SDL_MapRGB(this->image->format, r, g, b));

	return true;
}




