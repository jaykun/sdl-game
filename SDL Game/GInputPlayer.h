#pragma once
#ifndef GINPUTPLAYER_H_
#define GINPUTPLAYER_H_

#include <SDL.h>
#include <vector>
#include "Define.h"
#include "GEntity.h"
#include "GInput.h"

//Since SDL enums for buttons and axis overlap, a new one was needed to differentiate them
typedef enum
{
	CONTROLLER_BUTTON_INVALID = -1,
	CONTROLLER_BUTTON_A,
	CONTROLLER_BUTTON_B,
	CONTROLLER_BUTTON_X,
	CONTROLLER_BUTTON_Y,
	CONTROLLER_BUTTON_BACK,
	CONTROLLER_BUTTON_GUIDE,
	CONTROLLER_BUTTON_START,
	CONTROLLER_BUTTON_LEFTSTICK,
	CONTROLLER_BUTTON_RIGHTSTICK,
	CONTROLLER_BUTTON_LEFTSHOULDER,
	CONTROLLER_BUTTON_RIGHTSHOULDER,
	CONTROLLER_BUTTON_DPAD_UP,
	CONTROLLER_BUTTON_DPAD_DOWN,
	CONTROLLER_BUTTON_DPAD_LEFT,
	CONTROLLER_BUTTON_DPAD_RIGHT,	
	CONTROLLER_AXIS_TRIGGERLEFT,
	CONTROLLER_AXIS_TRIGGERRIGHT,
	CONTROLLER_AXIS_LEFTX_LEFT,
	CONTROLLER_AXIS_LEFTX_RIGHT,
	CONTROLLER_AXIS_LEFTY_UP,
	CONTROLLER_AXIS_LEFTY_DOWN,
	CONTROLLER_AXIS_RIGHTX_LEFT,
	CONTROLLER_AXIS_RIGHTX_RIGHT,
	CONTROLLER_AXIS_RIGHTY_UP,
	CONTROLLER_AXIS_RIGHTY_DOWN
} GameControllerButton;



class GInputPlayer : public GInput
{
public:
	GInputPlayer();
	GInputPlayer(SDL_GameController *gP);
	~GInputPlayer();

	//Get input instance through static function
	static GInputPlayer *GetInput(int number);

	//Loop these
	void OnInput(uint64_t current_time, float delta_time = 0);
	//void FlushBufferBefore(uint64_t time);

	

	//Keyboard
	void SetKeyboard(bool kb);
	static void UpdateKB();	

	//Gamepad
	uint8_t GetGamePadButton(GameControllerButton button, int deadzone = 0);
	void SetGamepad(SDL_GameController *gP);
	SDL_GameController *GetGamePad();
	void SetHaptic();
	void RemoveHaptic();
	void Rumble(float strength, int length);
	
	//Buffer access
	/*uint32_t *GetStateBuffer();
	uint64_t *GetTimestampBuffer();
	uint8_t GetBufferStart();
	uint8_t GetBufferIndex();*/

	//Cleanup
	void OnCleanUp();

	//Kb key bindings
	Uint8 KBLEFT = SDL_SCANCODE_LEFT;
	Uint8 KBRIGHT = SDL_SCANCODE_RIGHT;
	Uint8 KBUP = SDL_SCANCODE_UP;
	Uint8 KBDOWN = SDL_SCANCODE_DOWN;

	Uint8 KBRUN = SDL_SCANCODE_LSHIFT;
	Uint8 KBJUMP = SDL_SCANCODE_SPACE;
	Uint8 KBSTART = SDL_SCANCODE_ESCAPE;

	Uint8 KBATK;
	Uint8 KBATK2;

	Uint8 KBMENU;

private:
	bool kBController = true;
	static const Uint8 *keystate;

	SDL_GameController* gPController;
	SDL_Haptic* hapticDevice;
	bool rumble = true;

	static int count;
	static GInputPlayer *input[MAX_INPUTS];

	/*uint32_t state[256];
	uint64_t timestamp[256];

	Uint8 buffer_start = 0;
	Uint8 buffer_index = 0;

	uint32_t prev_button_state = 0;*/
};



#endif