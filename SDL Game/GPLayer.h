#pragma once
#ifndef GPLAYER_H_
#define GPLAYER_H_

#include "GEntity.h"
#include "GInputPlayer.h"
#include "GSound.h"
#include "GBufferReaderSpecial.h"


class GPlayer : public GEntity
{
public:
	//test
	friend class GState;

	GPlayer() = default;

	void OnInit(float delta_time);
	void OnLoop(float delta_time);
	void OnMove(float delta_time);
	void OnMove2(float delta_time);
	void OnRender(float extrap, SDL_Renderer *renderer);
	void OnCleanup();

	bool OnCollision(GEntity* Entity);

	bool GetGrounded();
	float GetSpeedX();
	float GetSpeedY();
	float GetFracX();
	float GetFracY();

	int GetRenderX(float extrapolate);
	int GetRenderY(float extrapolate);

	void MoveHor(float delta_time);

//protected:
	//movement
	int move = 0;

	float speed_x;
	float speed_y;

	float accel_x = 0;
	float accel_y = 0;

	float fraction_x = 0.0;
	float fraction_y = 0.0;

	float max_speed_x = 0.0;
	float max_speed_y = 0.0;

	//some values
	float grav = 0.75;
	float grav_max = 10;
	
	float walkspeed = 5;
	float walk_accel = 0.5;

	float runspeed = 10;
	float run_accel = 1;

	int jumpspeed = 12;

	//Time effects
	float timefactor = 1;

	//flags 
	bool grounded = false;
	bool jumping = false;

	float speed_x_next, speed_y_next;

	int x_prev, y_prev;

	//formerly private
//private:
	GBufferReaderSpecial br;
	uint32_t btn;
	uint32_t btn_pressed;
	uint32_t btn_released;
	uint32_t specials;

	GSound *jumpsound;

	bool PosValid(int NewX, int NewY);

	bool PosValidCol(int new_x, int new_y);
	bool TriangleCollision(SDL_Point a, SDL_Point b, SDL_Rect rect);

	bool PosValidEntity(GEntity* Entity, int NewX, int NewY);

	GState* state;
};

#endif