#ifndef DEFINE_H_
#define DEFINE_H_

#define MAP_WIDTH 40
#define MAP_HEIGHT 40

#define TILE_SIZE 16

#define WWIDTH 640
#define WHEIGHT 480

//If i remember correctly these don't exactly work
#define WMIDDLEH WWIDTH/2;
#define WMIDDLEV WHEIGHT/2;

#define MAX_CONTROLLERS 8
#define MAX_INPUTS 4
#define JOYSTICK_DEADZONE 8000

#endif