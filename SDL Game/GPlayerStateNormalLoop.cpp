#include "stdafx.h"
#include "GPlayerStateNormalLoop.h"
#include "GPlayerStateSpecialMove1.h"
#include "HelperFunctions.h"

#define Bit(a) 1U << a
#define GetBtn(a,b) (bool)(a & 1U << b)


GPlayerStateNormalLoop::GPlayerStateNormalLoop(GPlayer* player)
{
	m_player = player;
}

GState* GPlayerStateNormalLoop::OnInit(float delta_time)
{
	m_player->max_speed_x = 30 * delta_time;
	m_player->accel_x = 0;

	m_player->max_speed_y = 50 * delta_time;
	m_player->accel_y = m_player->grav * delta_time * m_player->timefactor;

	return nullptr;
}

GState* GPlayerStateNormalLoop::OnLoop(float delta_time)
{
	//specials test
	if (GetBtn(m_player->specials, 0))
	{
		std::cout << "hadouken" << std::endl;
		return new GPlayerStateSpecialMove1(m_player);
	}

	/*if (GetBtn(m_player->specials, 16))
	{
		std::cout << "hold" << std::endl;
		m_player->texture_red = 0;
	}
	else
	{
		m_player->texture_red = 255;
	}

	if (GetBtn(m_player->specials, 24))
	{
		std::cout << "repeat" << std::endl;	
		m_player->texture_blue = 0;
	}
	else
	{
		m_player->texture_blue = 255;
	}*/


	//m_player->max_speed_y = 50;
	m_player->accel_y = m_player->grav * delta_time * m_player->timefactor;

	//Get direction of horizontal movement
	//move = -input->leftButton + input->rightButton;
	m_player->move = -GetBtn(m_player->btn, BUTTON_LEFT) + GetBtn(m_player->btn, BUTTON_RIGHT);
	
	//Very simple script for basic movement
	if (m_player->grounded)
	{
		//if (input->jumpButtonPressed)
		if (GetBtn(m_player->btn_pressed, BUTTON_A))
		{
			m_player->jumping = true;
			m_player->speed_y = -m_player->jumpspeed;
			m_player->accel_y = 0.0;
			//jump sound, works but annoying
			//jumpsound->Play(1);					
		}
	}
	else
	{
		if (m_player->jumping)
		{
			//if (input->jumpButtonReleased == 1 && speed_y < 0)
 			if (GetBtn(m_player->btn_released, BUTTON_A) && m_player->speed_y < 0)
			{
				m_player->jumping = false;
				m_player->speed_y = 0;
				m_player->accel_y = 0.0;
			}
		}
	}

	m_player->MoveHor(delta_time);

	//Animation
	m_player->Anim_Control.OnAnimate(delta_time, m_player->timefactor);

	//Move character, check collisions
	m_player->OnMove(delta_time);

	return nullptr;
}

void GPlayerStateNormalLoop::OnEnd()
{

}