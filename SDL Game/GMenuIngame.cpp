#include "stdafx.h"
//GameMaster included here instead of GMenuIngame.h to avoid circular #include
#include "GameMaster.h"
#include "GMenuIngame.h"


void GMenuIngame::OnInit(GameMaster *instance, TTF_Font *font)
{
	game = instance;
	menu_font = font;

	for (int i = 0; i < 4; ++i)
	{
		input[i] = GInputPlayer::GetInput(i);
	}

	br.SetInput(GInputPlayer::GetInput(0));

	border_x = WWIDTH/2 - 100;
	border_y = WHEIGHT/2 - 100;
}

void GMenuIngame::OnLoop()
{
	//Check if menu button pressed on KB or Gamepad
	bool menu_pressed = false;

	//TODO: menu functions will need also hold state, probably release too
	uint32_t btn_pressed = 0;
	for (int i = 0; i < MAX_INPUTS; i++)
	{
		br.SetInput(GInputPlayer::GetInput(i));
		br.ReadInput();
		btn_pressed |= br.GetStatePressed();
		menu_pressed |= btn_pressed & (1U << BUTTON_START);
	}


	paused_prev = paused;

	if (menu_pressed)
	{		
		paused = !paused;
		//pause game loop starting next frame
		game->SetLoop(!paused);
		//if game is unpaused, we need to set rendering loop back on, starting this frame
		game->SetRender(true);
	}

	if (paused)
	{
		//Handle Input
		bool up_pressed = false;
		bool down_pressed = false;

		bool left_pressed = false;
		bool right_pressed = false;

		bool confirm_pressed = false;
		bool cancel_pressed = false;

		//Input for pause menu is an aggregate of all controllers + keyboard
		for (int i = 0; i < MAX_INPUTS; ++i)
		{
			up_pressed = btn_pressed & (1U << BUTTON_UP);
			down_pressed = btn_pressed & (1U << BUTTON_DOWN);
			left_pressed = btn_pressed & (1U << BUTTON_LEFT);
			right_pressed = btn_pressed & (1U << BUTTON_RIGHT);

			//confirm_pressed |= input[i]->confirmButtonPressed;
			//cancel_pressed |= input[i]->cancelButtonPressed;
		}

		//direction of movement for horizontal and vertical
		int dir_h = right_pressed - left_pressed;
		int dir_v = down_pressed - up_pressed;

		//Move menu accordingly		
		current_item += dir_v;

		if (current_item >= max_items)
		{
			current_item = 0;
		}
		else if (current_item < 0)
		{
			current_item = max_items-1;
		}

		//TODO: actual functionality beyond moving through the menu items
	}
}

void GMenuIngame::OnRenderBegin(SDL_Renderer *renderer)
{
	//If paused and previously not, create a pause texture and set it as render target in main render loop
	//thus the pause texture is created
	if (paused && paused_prev == false)
	{
		//for some reason this is needed even though this is set in OnLoop()
		game->SetRender(true);

		//If texture doesn't exist yet, create it.
		if (pause_texture.GetTexture() == nullptr)
		{
			if (!pause_texture.OnLoadBlank(WWIDTH, WHEIGHT, SDL_TEXTUREACCESS_TARGET, renderer))
			{
				printf("failed to create empty texture!");
				return;
			}
		}

		//Set texture as rendering target and clear the texture with colour black
		SDL_SetRenderTarget(renderer, pause_texture.GetTexture());
		SDL_SetRenderDrawColor(renderer, 0x0, 0x0, 0x0, 0xFF);
		SDL_RenderClear(renderer);
	}
}

//As this is after the normal rendering loop, OnRenderEnd() might be more appropriate
void GMenuIngame::OnRender(SDL_Renderer *renderer)
{
	//If we are paused, we set all other rendering off and render the pause screen only
	if (paused)
	{
		game->SetRender(false);
		//std::cout << "Render = FALSE" << std::endl;
		//Create menu box rect
		SDL_Rect rect;

		rect.x = border_x;
		rect.y = border_y;
		rect.w = border_width;
		rect.h = border_height;

		//Render additional items to pause_screen
		SDL_SetRenderTarget(renderer, pause_texture.GetTexture());

		//Black filled rect		
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // the rect color (black)
		SDL_RenderFillRect(renderer, &rect);

		//Red borders to previous rect
		SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // the rect color (red)
		SDL_RenderDrawRect(renderer, &rect);

		//Add text		
		int middle_x = WWIDTH / 2;		

		std::string items[4];
		items[0] = "Resume";
		items[1] = "Options";
		items[2] = "Exit to Title";
		items[3] = "Quit Game";

		SDL_Color c = { 255, 0, 0 };

		for (int i = 0; i < 4; i++)
		{
			//highlight selected menu item with color
			if (i == current_item)
			{
				c = { 255, 255, 255 };
				
			}
			else
			{
				c = { 255, 0, 0 };
			}

			pause_texture.AddText(renderer, menu_font, middle_x, border_y + 25 + 50 * i, items[i], 20, c, 0, SDL_FLIP_NONE);
		}

		//Stop rendering to pause texture
		SDL_SetRenderTarget(renderer, nullptr);

		//Texture is now ready to render to screen
		if (pause_texture.GetTexture() != NULL)
		{
			pause_texture.OnRender(
				0,
				0,
				0,
				0,
				WWIDTH,
				WHEIGHT,
				renderer
			);
		}
	}
}