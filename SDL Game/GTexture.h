#pragma once
#ifndef GTEXTURE_H_
#define GTEXTURE_H_
#include <iostream>
#include <string>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <unordered_map>


class GTexture
{
public:
	~GTexture();

	//File loading	
	bool OnLoad(std::string path, SDL_Renderer* renderer);
	bool OnLoadText(std::string path, std::string textureText, int size, SDL_Color textColor, SDL_Renderer* renderer);
	bool OnLoadBlank(int width, int height, SDL_TextureAccess access, SDL_Renderer* renderer);
	bool GetInfo(std::string meta);

	//Add text to texture
	bool AddText(SDL_Renderer* renderer, TTF_Font* font, int x, int y, std::string textureText, int size, SDL_Color textColor, float angle, SDL_RendererFlip flip);

	//Cleanup
	void Free();

	//Set color modulation
	void SetColor(Uint8 red, Uint8 green, Uint8 blue);

	//Set blending
	void SetBlendMode(SDL_BlendMode blending);

	//Set alpha modulation
	void SetAlpha(Uint8 alpha);

	//Renders texture at given point
	void OnRender(int x, int y, int x2, int y2, int w, int h, SDL_Renderer* renderer, float angle = 0.0f, SDL_Point point = SDL_Point{ 0, 0 }, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//Renders texture at given point with modifiers
	void OnRenderMod(int x, int y, int x2, int y2, int w, int h, SDL_Renderer* renderer, float angle = 0.0f, SDL_Point point = SDL_Point{0, 0}, SDL_RendererFlip flip = SDL_FLIP_NONE,
		SDL_BlendMode blending = SDL_BLENDMODE_BLEND, Uint8 r = 255, Uint8 g = 255, Uint8 b = 255, Uint8 a = 255);

	//Getters
	SDL_Texture* GetTexture();

	static GTexture* GetTexture(const std::string);

	Uint32 GetWidth();
	Uint32 GetHeight();

	//Texture settings

	Uint32 width = 0;
	Uint32 height = 0;	

	Uint8 r_default = 255;
	Uint8 g_default = 255;
	Uint8 b_default = 255;
	Uint8 a_default = 255;

	SDL_BlendMode blend_default = SDL_BLENDMODE_BLEND;

	//Extra information
	Uint32 frame_width = 0;
	Uint32 frame_height = 0;

	Uint32 col_type = 0;

	Uint32 col_x = 0;
	Uint32 col_y = 0;
	Uint32 col_width = 0;
	Uint32 col_height = 0;

	Uint32 max_frames = 0;

private:
	SDL_Texture *texture = NULL;	

	//holds all succesfully loaded textures
	static std::unordered_map<std::string, GTexture*> texture_list;

};






#endif