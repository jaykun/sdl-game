#include "stdafx.h"
#include "GEvent.h"
#include <iostream>
#include <string>

GEvent::GEvent() {}

GEvent::~GEvent() {}

void GEvent::OnEvent(SDL_Event* Event)
{
	switch (Event->type)
	{
	case SDL_WINDOWEVENT:
	{
		switch (Event->window.event)
		{
		case SDL_WINDOWEVENT_ENTER:
		{
			OnMouseFocus();
			break;
		}
		case SDL_WINDOWEVENT_LEAVE:
		{
			OnMouseBlur();
			break;
		}
		case SDL_WINDOWEVENT_FOCUS_GAINED:
		{
			OnInputFocus();
			break;
		}
		case SDL_WINDOWEVENT_FOCUS_LOST:
		{
			OnInputBlur();
			break;
		}
		case SDL_WINDOWEVENT_SHOWN:
		{
			break;
		}
		case SDL_WINDOWEVENT_HIDDEN:
		{
			break;
		}
		case SDL_WINDOWEVENT_EXPOSED:
		{
			break;
		}
		case SDL_WINDOWEVENT_MOVED:
		{
			break;
		}
		case SDL_WINDOWEVENT_RESIZED:
		{
			OnWindowResized(Event->window.data1, Event->window.data2);
			break;
		}
		case SDL_WINDOWEVENT_SIZE_CHANGED:
		{
			break;
		}
		case SDL_WINDOWEVENT_MINIMIZED:
		{
			break;
		}
		case SDL_WINDOWEVENT_MAXIMIZED:
		{
			break;
		}
		case SDL_WINDOWEVENT_RESTORED:
		{
			break;
		}
		case SDL_WINDOWEVENT_CLOSE:
		{
			break;
		}
		}
		break;
	}
	case SDL_QUIT:
	{
		OnExit();
		break;
	}
	case SDL_KEYDOWN:
	{
		OnKeyDown(Event->key.keysym.sym, Event->key.repeat);
		break;
	}
	case SDL_KEYUP:
	{
		OnKeyUp(Event->key.keysym.sym);
		break;
	}
	case SDL_MOUSEMOTION:
	{
		OnMouseMove(Event->motion.x, Event->motion.y, Event->motion.xrel, Event->motion.yrel, (Event->motion.state&SDL_BUTTON(SDL_BUTTON_LEFT)) != 0, (Event->motion.state&SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0, (Event->motion.state&SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0);
		break;
	}
	case SDL_MOUSEBUTTONDOWN:
	{
		switch (Event->button.button)
		{
		case SDL_BUTTON_LEFT:
		{
			OnLButtonDown(Event->button.x, Event->button.y);
			break;
		}
		case SDL_BUTTON_RIGHT:
		{
			OnRButtonDown(Event->button.x, Event->button.y);
			break;
		}
		case SDL_BUTTON_MIDDLE:
		{
			OnMButtonDown(Event->button.x, Event->button.y);
			break;
		}
		}
		break;
	}
	case SDL_MOUSEBUTTONUP:
	{
		switch (Event->button.button)
		{
		case SDL_BUTTON_LEFT:
		{
			OnLButtonUp(Event->button.x, Event->button.y);
			break;
		}
		case SDL_BUTTON_RIGHT:
		{
			OnRButtonUp(Event->button.x, Event->button.y);
			break;
		}
		case SDL_BUTTON_MIDDLE:
		{
			OnMButtonUp(Event->button.x, Event->button.y);
			break;
		}
		}
		break;
	}

	
	case SDL_JOYAXISMOTION:
	{
		OnJoyAxis(Event->jaxis.which, Event->jaxis.axis, Event->jaxis.value);
		break;
	}
	case SDL_JOYBALLMOTION:
	{
		OnJoyBall(Event->jball.which, Event->jball.ball, Event->jball.xrel, Event->jball.yrel);
		break;
	}
	case SDL_JOYHATMOTION:
	{
		OnJoyHat(Event->jhat.which, Event->jhat.hat, Event->jhat.value);
		break;
	}
	case SDL_JOYBUTTONDOWN:
	{
		OnJoyButtonDown(Event->jbutton.which, Event->jbutton.button);
		break;
	}
	case SDL_JOYBUTTONUP:
	{
		OnJoyButtonUp(Event->jbutton.which, Event->jbutton.button);
		break;
	}
	case SDL_CONTROLLERDEVICEADDED:
	{
		OnControllerDeviceAdded(Event->cdevice.type, Event->cdevice.which);
		break;
	}
	case SDL_CONTROLLERDEVICEREMOVED:
	{
		OnControllerDeviceRemoved(Event->cdevice.type, Event->cdevice.which);
		break;
	}
	case SDL_CONTROLLERBUTTONDOWN:
	{
		OnControllerButtonDown(Event->cbutton.button, Event->cdevice.which);
		break;
	}
	case SDL_CONTROLLERBUTTONUP:
	{
		OnControllerButtonUp(Event->cbutton.button, Event->cdevice.which);
		break;
	}
	case SDL_CONTROLLERAXISMOTION:
	{
		OnControllerAxisMotion(Event->caxis.axis, Event->caxis.value, Event->caxis.which);
		break;
	}
	default:
	{
		OnUser(Event->user.type, Event->user.code, Event->user.data1, Event->user.data2);
		break;
	}
	}
}

void GEvent::OnExit() {}

void GEvent::OnKeyDown(SDL_Keycode sym/*, Uint16 mod*/, Uint16 repeat)
{
	//printf("keycode:%d\tmod:%d\tscancode:%d pressed!\n", sym, mod, scancode);
}

void GEvent::OnKeyUp(SDL_Keycode sym/*, Uint16 mod*/)
{
	//printf("keycode:%d\tmod:%d\tscancode:%d unpressed!\n", sym, mod, scancode);
}

void GEvent::OnMouseMove(int mx, int my, int relx, int rely, bool Left, bool Right, bool Middle)
{
	//printf("x:%d\ty:%d\n", mx, my);
}

void GEvent::OnLButtonDown(int mx, int my)
{
	//printf("Left Click pressed in x:%d\ty:%d\n", mx, my);
}

void GEvent::OnLButtonUp(int mx, int my)
{
	//printf("Left Click unpressed in x:%d\ty:%d\n", mx, my);
}

void GEvent::OnRButtonDown(int mx, int my)
{
	//printf("Right Click pressed in x:%d\ty:%d\n", mx, my);
}

void GEvent::OnRButtonUp(int mx, int my)
{
	//printf("Right Click unpressed in x:%d\ty:%d\n", mx, my);
}

void GEvent::OnMButtonDown(int mx, int my)
{
	//printf("Middle Click pressed in x:%d\ty:%d\n", mx, my);
}

void GEvent::OnMButtonUp(int mx, int my)
{
	//printf("Middle Click unpressed in x:%d\ty:%d\n", mx, my);
}

void GEvent::OnMouseFocus()
{
	//printf("Mouse entered the window!\n");
}

void GEvent::OnMouseBlur()
{
	//printf("Mouse leaved the window!\n");
}

void GEvent::OnInputFocus()
{
	//printf("Keyboard Focus the window!\n");
}

void GEvent::OnInputBlur()
{
	//printf("Keyboard losed focus on the window!\n");
}

void GEvent::OnWindowResized(int data1, int data2)
{
	//printf("Window Resized to: width:%d\theight:%d\n", data1, data2);
}

void GEvent::OnJoyAxis(Uint8 which, Uint8 axis, Sint16 value) {}

void GEvent::OnJoyBall(Uint8 which, Uint8 ball, Sint16 xrel, Sint16 yrel) {}

void GEvent::OnJoyHat(Uint8 which, Uint8 hat, Uint8 button) {}

void GEvent::OnJoyButtonDown(Uint8 which, Uint8 button) {}

void GEvent::OnJoyButtonUp(Uint8 which, Uint8 button) {}

void GEvent::OnControllerDeviceAdded(Uint32 type, int which) {}

void GEvent::OnControllerDeviceRemoved(Uint32 type, int which) {}

void GEvent::OnControllerButtonDown(Uint8 button, int which) {}

void GEvent::OnControllerButtonUp(Uint8 button, int which) {}

void GEvent::OnControllerAxisMotion(Uint8 axis, Sint16 value, int which) {}

void GEvent::OnUser(Uint8 type, int code, void* data1, void* data2) {}