#include "stdafx.h"
#include "GInputYoshiAI.h"

GInputYoshiAI::GInputYoshiAI(GYoshi* yoshi)
{
	m_yoshi = yoshi;

	m_start_x = m_yoshi->x;
	dist_to_run = 100;
	dir = 1;
	dest_x = m_start_x + dir * dist_to_run;
}

void GInputYoshiAI::Init()
{

}

//not sure if we should just put enemyai in OnLoop. Would solve all the timing issues
void GInputYoshiAI::OnInput(uint64_t current_time, float delta_time)
{
	uint32_t button_state = 0;

	//timer should be multiplied by step_interval, maybe even time_factor so it would be fps independent
	timer += 1.0f * delta_time;

	if (m_yoshi->jumping)
		button_state += 1U << BUTTON_A;


	if (timer > 60.0f)
	{
		if (m_yoshi->grounded)
		{
			button_state += 1U << BUTTON_A;
		}
		timer = 0.0f;
	}


	if (dir == 1 && m_yoshi->x < dest_x ||
		dir == -1 && m_yoshi->x > dest_x)
	{
		if (dir == 1)
			button_state += 1U << BUTTON_RIGHT;
		else
			button_state += 1U << BUTTON_LEFT;
	}
	else
	{
		dir *= -1;
		dest_x = m_start_x + dir * dist_to_run;
	}

	//since we're in OnLoop, current_time doesn't matter.
	//In OnInput(), this should be back as function argument
	//Can't change argument as function is virtual, must change class.
	current_time = 0;

	Uint8 lim = buffer_start - 1;
	if (button_state != prev_button_state && buffer_index != lim)
	{
		//add state to buffer
		state[buffer_index] = button_state;
		//add timestamp
		timestamp[buffer_index] = current_time;

		//increment index, so we don't overwrite the same location continuously in array
		buffer_index++;

		//save this button state so we can compare if the next state differs from current
		prev_button_state = button_state;
	}
}