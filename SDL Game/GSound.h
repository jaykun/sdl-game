#pragma once
#ifndef GSOUND_H_
#define GSOUND_H_

#include "SDL.h"
#include <SDL_mixer.h>
#include <iostream>
#include <unordered_map>

class GSound
{
public:
	GSound() = default;
	GSound(std::string path);
	static GSound *GSound::GetSound(const std::string name);
	void OnLoad(std::string path);
	void Free();
	~GSound();

	void SetChannel(int ch);
	int GetChannel();

	void Play(int loops);

private:
	int channel = -1;
	Mix_Chunk *sound;

	static std::unordered_map<std::string, GSound*> sound_list;
};


#endif
