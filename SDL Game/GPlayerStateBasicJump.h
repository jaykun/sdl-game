#pragma once
#ifndef GPLAYERSTATEBASICJUMP_H_
#define GPLAYERSTATEBASICJUMP_H_

#include "GState.h"

class GPlayerStateBasicJump : public GState
{
public:
	GPlayerStateBasicJump(GPlayer* player);// = default;
	virtual ~GPlayerStateBasicJump() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif