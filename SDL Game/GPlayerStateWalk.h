#pragma once
#ifndef GPLAYERSTATEWALK_H_
#define GPLAYERSTATEWALK_H_

#include "GPlayerStateGround.h"

class GPlayerStateWalk : public GState
{
public:
	GPlayerStateWalk(GPlayer* player);// = default;
	virtual ~GPlayerStateWalk() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif