#pragma once
#ifndef GINCLUDEHOLD_H_
#define GINCLUDEHOLD_H_

#include <cstdint>

class GInputHold
{
public:
	GInputHold() = default;
	~GInputHold() = default;
	void SetTimelimit(uint64_t min_ms);
	void SetButton(uint32_t button);
	bool Handle(uint32_t state, uint32_t state_pressed, uint32_t state_released);
	void UpdateTime(uint64_t time_);
	void Active(bool val);
	bool Active();
private:
	uint64_t time = 0;
	uint64_t min_ms = 0;
	uint64_t min_time = UINT64_MAX;
	uint32_t button = 0;
	bool active = false;
};


#endif
