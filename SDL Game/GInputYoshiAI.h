#pragma once
#ifndef GINPUTYOSHIAI_H_
#define GINPUTYOSHIAI_H_

#include <SDL.h>
#include "GInput.h"
#include "GYoshi.h"
#include <chrono>

class GInputYoshiAI : public GInput
{
public:
	GInputYoshiAI(GYoshi* yoshi);
	void Init();
	~GInputYoshiAI() = default;

	virtual void OnInput(uint64_t current_time, float delta_time);

protected:
	GYoshi* m_yoshi;

	int m_start_x;
	int dist_to_run;
	int dir;
	int dest_x;
	bool update_needed = true;
	float timer;
	bool prev_frame = false;
};

#endif