#include "stdafx.h"
#include "GInputSequence.h"

void GInputSequence::SetTimelimit(uint64_t max_ms)
{
	this->max_ms = max_ms;
}

void GInputSequence::SetSequence(uint32_t button)
{
	final_cmd++;
	this->sequence[final_cmd] = button;
}

bool GInputSequence::Handle(uint32_t state, uint32_t state_pressed, uint32_t state_released, uint64_t time)
{
	if (time > max_time)
	{
		seqnum = 0;
		max_time = UINT32_MAX;		
	}

	if (state_pressed & (1U << sequence[seqnum]))
	{
		switch (seqnum)
		{
		case 0:
			//std::cout << "number " << seqnum << std::endl;
			max_time = time + max_ms;
			seqnum++;
			break;
		default:
			if (time <= max_time)
			{
				if (seqnum == final_cmd)
				{
					//std::cout << "complete" << std::endl;
					seqnum = 0;
					max_time = UINT32_MAX;
					return true;
				}
				else
				{
					//std::cout << "number " << seqnum << std::endl;
					seqnum++;
				}
			}

			break;
		}
	}

	return false;
}

void GInputSequence::Active(bool val)
{
	this->active = val;
}

bool GInputSequence::Active()
{
	return active;
}