#include "stdafx.h"
#include "GBufferReaderSpecial.h"

GBufferReaderSpecial::GBufferReaderSpecial()
{
	input = GInputPlayer::GetInput(0);
	//TODO: Read special moves from file
	//100ms = 100000

	sequence[0].SetTimelimit(200000);
	sequence[0].SetSequence(BUTTON_DOWN);
	sequence[0].SetSequence(BUTTON_RIGHT);
	sequence[0].SetSequence(BUTTON_A);
	sequence[0].Active(true);

	repeat[0].SetTimelimit(150000);
	repeat[0].SetButton(BUTTON_DOWN);
	repeat[0].Active(true);

	hold[0].SetTimelimit(5000000);
	hold[0].SetButton(BUTTON_DOWN);
	hold[0].Active(true);
}

void GBufferReaderSpecial::ReadInput()
{
	if (input == nullptr)
		return;

	//cache time from each input state here
	//should be 64_t but this doesn't work for some reason, needs study
	uint64_t time = 0;

	//these need to be reset each frame
	state_pressed = 0;
	state_released = 0;
	state_special = 0; // &= (255U << 16);

	/*for (int i = 0; i < 32; i++)
	{
		std::cout << ((int)(bool)(state_special & (1U << i)));
	}
	std::cout << std::endl;*/

	uint8_t buff_start = input->GetBufferStart();
	uint8_t buff_index = input->GetBufferIndex();

	//determine whether holds are valid based on time
	for (int i = 0; i < 8; i++)
	{
		if (!hold[i].Active())
			break;
		hold[i].UpdateTime(current_time);
	}

	bool holds_updated = false;

	//iterate buffer from start to latest, or until current time is reached
	for (uint8_t buff_iter = buff_start; time <= current_time && buff_iter != buff_index; buff_iter++)
	{
		//simple input
  		prev_state = state;
		state = input->GetStateBuffer()[buff_iter];
		uint64_t state_change = state ^ prev_state;

		//for use with advanced input
		uint32_t state_pressed_temp = state_change & state;
		uint32_t state_released_temp = state_change & (~state);

		//for use with simple input
		state_pressed |= state_pressed_temp;
		state_released |= state_released_temp;

		//for use with advanced input and current loop
		//should be 64_t but this doesn't work for some reason, needs study
		time = input->GetTimestampBuffer()[buff_iter];

		//input sequences update, bits 0-15 of state_special uint
		for (int i = 0; i < 16; i++)
		{
			if (!sequence[i].Active())
				break;
			if (sequence[i].Handle(state, state_pressed_temp, state_released_temp, time))
				state_special += 1U << i;
		}
		
		//input holds update, bits 16-23 of state_special uint
		for (int i = 0; i < 8; i++)
		{
			if (!hold[i].Active())
				break;			
			if (hold[i].Handle(state, state_pressed_temp, state_released_temp))
				state_special += 1U << (i + 16);
		}
		holds_updated = true;

		//input repeats update, bits 24-31 of state_special uint
		for (int i = 0; i < 8; i++)
		{
			if (!repeat[i].Active())
				break;
			repeat[i].Handle(state_pressed_temp);
		}
	}

	if (!holds_updated)
	{
		for (int i = 0; i < 8; i++)
		{
			if (!hold[i].Active())
				break;
			if (hold[i].Handle(0, 0, 0))
				state_special += 1U << (i + 16);
		}
	}
	//std::cout << state_special << std::endl;

	//determine whether repeats are valid based on time
	for (int i = 0; i < 8; i++)
	{
		if (!repeat[i].Active())
			break;
		if (repeat[i].UpdateTime(current_time))
			state_special += 1U << (i + 24);
	}
}

uint32_t GBufferReaderSpecial::GetStateSpecial()
{
	return state_special;
}