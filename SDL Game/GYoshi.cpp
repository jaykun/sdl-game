#include "stdafx.h"
#include "GYoshi.h"
#include "GameMaster.h"
#include "HelperFunctions.h"
#include "GInputYoshiAI.h"
#include "Collision.h"

#define GetBtn(a,b) (bool)(a & 1U << b)

void GYoshi::OnInit(GInputYoshiAI* ai)
{
	br.SetInput(ai);
	OnLoad(*GTexture::GetTexture("yoshi.png"));
	jumpsound = GSound::GetSound("low.wav");
	type = COL_SOLID;
}

bool GYoshi::OnLoad(GTexture& texture_)
{
	this->texture = &texture_;

	//collider = new GCollider(texture_.col_x, texture_.col_y, texture_.col_width, texture_.col_height, 0, 0, SHAPE_RECTANGLE, TYPE_SOLID);

	this->width = texture_.frame_width;
	this->height = texture_.frame_height;

	this->col_x[0] = texture_.col_x;// -texture_.center.x;
	this->col_y[0] = texture_.col_y;// -texture_.center.y;
	this->col_x[1] = texture_.col_width;
	this->col_y[1] = texture_.col_height;

	this->Anim_Control.MaxFrames = texture_.max_frames;
	return true;
}


void GYoshi::OnLoop(float delta_time)
{
	if (!loop)
		return;

	br.ReadInput();

	//get button states
	btn = br.GetState();
	btn_pressed = br.GetStatePressed();
	btn_released = br.GetStateReleased();

	max_speed_y = 50;
	//NOTE: it feels like using delta time in acceleration doesn't work in delta_time outside 1
	accel_y = grav * delta_time * timefactor;

	//Get direction of horizontal movement
	//move = -input->leftButton + input->rightButton;
	move = -GetBtn(btn, BUTTON_LEFT) + GetBtn(btn, BUTTON_RIGHT);

	//Very simple script for basic movement
	if (grounded)
	{
		//if (input->jumpButtonPressed)
		if (GetBtn(btn_pressed, BUTTON_A))
		{
			jumping = true;
			speed_y = -jumpspeed;
			accel_y = 0.0;
			//jump sound, works but annoying
			//jumpsound->Play(1);					
		}
	}
	else
	{
		if (jumping)
		{
			//if (input->jumpButtonReleased == 1 && speed_y < 0)
			if (GetBtn(btn_released, BUTTON_A) && speed_y < 0)
			{
				jumping = false;
				speed_y = 0;
				accel_y = 0.0;
			}
		}
	}

	MoveHor(delta_time);

	//Animation
	Anim_Control.OnAnimate(delta_time, timefactor);

	//Move character, check collisions
	OnMove(delta_time);
}

void GYoshi::MoveHor(float delta_time)
{
	float accel_x_amount;

	//if (input->runButton)
	if (GetBtn(btn, BUTTON_RT))
	{
		max_speed_x = runspeed;
		accel_x_amount = run_accel;
		Anim_Control.SetFrameRate(0.2 * 1);
	}
	else
	{
		max_speed_x = walkspeed;
		accel_x_amount = walk_accel;
		Anim_Control.SetFrameRate(0.1 * 1);
	}

	switch (move)
	{
	case -1:
		//NOTE: it feels like using delta time in acceleration doesn't work in delta_time outside 1
		accel_x = -accel_x_amount * delta_time * timefactor;

		//turn sprite left
		Anim_Control.CurrentFrameCol = 0;
		break;
	case 1:
		//NOTE: it feels like using delta time in acceleration doesn't work in delta_time outside 1
		accel_x = accel_x_amount * delta_time * timefactor;

		//turn sprite right
		Anim_Control.CurrentFrameCol = 1;
		break;
	default:
		Anim_Control.SetFrameRate(0);
		Anim_Control.SetCurrentFrame(0);

		//if we can decelarate without changing direction
		if (abs(speed_x) > accel_x_amount)
		{
			int speed_x_sign = signOf(speed_x);
			//NOTE: it feels like using delta time in acceleration doesn't work in delta_time outside 1
			accel_x = -speed_x_sign * accel_x_amount * delta_time * timefactor;

			//alternative
			//if (abs(accel_x/2) >= abs(speed_x))

			//If deceleration caused speed sign to change, set speed and accel to 0
			if (signOf(speed_x + accel_x / 2) != speed_x_sign)
			{
				speed_x = 0;
				accel_x = 0;
			}
		}
		else
		{
			speed_x = 0;
			accel_x = 0;
		}

		break;
	}
}


//This function is designed so movement and collision are accurate even with high delta
//fractions of x and y coordinate are stored in separate variables
//only when the (abs) fraction variable is greater than 1 does the character actually move
void GYoshi::OnMove(float delta_time)
{
	//since gravity is always present, we are always moving and this is quite moot.
	//might be useful in some special cases though
	if (!((bool)speed_x | (bool)speed_y | (bool)accel_x | (bool)accel_y)) return;
	//buggy for some reason
	//if (!(bool)(speed_x + speed_y + accel_x + accel_y)) return;

	//for accurate simulation: accel = amount * dt => pos = (speed + accel/2) * dt => speed = speed + accel
	//get the speed calculated with deltatime and timefactor, clamped to maximum speed set earlier
	float move_x = clamp((speed_x + accel_x / 2), -max_speed_x, max_speed_x) * delta_time * timefactor;
	float move_y = clamp((speed_y + accel_y / 2), -max_speed_y, max_speed_y) * delta_time * timefactor;

	//test, can be removed
	x_prev = x;
	y_prev = y;

	//for accurate simulation: accel = amount * dt => pos = (speed + accel/2) * dt => speed = speed + accel
	//Since position is technically updated above using old speed, we can do this here
	speed_x = clamp(speed_x + accel_x, -max_speed_x, max_speed_x);
	speed_y = clamp(speed_y + accel_y, -max_speed_y, max_speed_y);

	//For use in MVC rendering
	//x_interp = move_x;
	//y_interp = move_y;

	//test, reset values
	speed_x_next = 0;
	speed_y_next = 0;

	//sign of movement
	int move_sign_x = signOf(move_x);
	int move_sign_y = signOf(move_y);

	//how much should be moved in a single frame
	float move_step_x, move_step_y;

	//if there's horizontal movement
	if (move_sign_x)
	{
		//get ratio between x/y movement amount
		float ratio = abs(move_x / move_y);

		//if y movement is zero, set ratio to 1 instead of 0
		if (!ratio)
			ratio = 1;

		//calculate single step movement, set it so that x and y movement reach their target simultaneously
		move_step_x = move_sign_x * delta_time * timefactor * ratio;
	}

	if (move_sign_y)
	{
		//calculate single step movement
		move_step_y = move_sign_y * delta_time * timefactor;
	}

	while (true)
	{
		if (move_x)
		{
			//move_step_x will be replaced by move_x if move_x is smaller
			move_step_x = move_sign_x * std::min(abs(move_step_x), abs(move_x));
			//remove move_step_x from move_x. at the end of movement move_x will be zero
			move_x -= move_step_x;

			//add the removed part to fraction_x
			fraction_x += move_step_x;

			//if (abs)fraction is greater or eq to 1, move in that direction
			for (; abs(fraction_x) >= 1; fraction_x -= move_sign_x)
			{
				//if there's nothing in the way, move forward
				if (!CollisionAtPlace(this, x + move_sign_x, y, GWall::GWallList))
				//if (PosValid(x + move_sign_x, y))
				{
					x += move_sign_x;
				}
				else
				{
					//if we run into a wall, set all movement variables to 0
					speed_x = 0;
					accel_x = 0;

					move_x = 0;
					fraction_x = 0;

					//x_interp = 0;
					break;
				}
			}

			//exit loop if no more movement left
			if (!move_x && !move_y)
			{
				break;
			}
		}

		if (move_y)
		{
			//move_step_y will be replaced by move_y if move_y is smaller
			move_step_y = move_sign_y * std::min(abs(move_step_y), abs(move_y));
			//remove move_step_y from move_y. at the end of movement move_y will be zero
			move_y -= move_step_y;
			//add the removed part to fraction_y
			fraction_y += move_step_y;

			//if (abs)fraction is greater or eq to 1, move in that direction
			for (; abs(fraction_y) >= 1; fraction_y -= move_sign_y)
			{
				//if there's nothing in the way, move forward
				if (!CollisionAtPlace(this, x, y + move_sign_y, GWall::GWallList))
				//if (PosValid(x, y + move_sign_y))
				{
					//since moving horizontally means we are in the air => grounded is 0
					grounded = false;
					y += move_sign_y;
				}
				else
				{
					//if we are moving downwards and encounter a wall, we are now standing on said wall
					if (move_sign_y == 1)
					{
						//camera shake and gamepad rumble test
						if (!grounded)
						{
							//GCamera::CameraControl.Shake(delta_real, 5, 5, 3);
							//input->Rumble(0.5, 200);
						}

						grounded = true;
						jumping = false;

					}

					//if we run into a wall, set all movement variables to 0
					speed_y = 0;
					accel_y = 0;

					move_y = 0;
					fraction_y = 0;

					//y_interp = 0;
					break;
				}
			}

			//exit loop if no more movement left
			if (!move_x && !move_y)
			{
				break;
			}
		}
	}

	//Calculating extrapolation values with new speed
	//This is a bit troublesome with high speeds as moving inside walls is quite possible (e.g. speed of 400)
	//Have to look into interpolation instead of extrapolation, possibly
	speed_x_next = clamp((speed_x + accel_x / 2), -max_speed_x, max_speed_x) * delta_time * timefactor;

	//We are not moving anywhere if we are grounded.
	if (!grounded)
	{
		speed_y_next = clamp((speed_y + accel_y / 2), -max_speed_y, max_speed_y) * delta_time * timefactor;
	}
}


bool GYoshi::PosValid(int NewX, int NewY)
{
	bool Return = true;

	Return = PosValidCol(NewX, NewY);

	//Check if touching entities
	//If player can't be touched, skip this section
	if (!(flags & ENTITY_FLAG_MAPONLY))
	{
		//Check against all existing entities
		for (int i = 0; i < entityList.size(); i++)
		{
			if (PosValidEntity(entityList[i], NewX, NewY) == false)
			{
				Return = false;
			}
		}
	}

	//If Return was not set to false, new position is valid
	return Return;
}

bool GYoshi::PosValidCol(int new_x, int new_y)
{
	//if sprite has collisions defined. this is better and should probably be used
	int own_left = new_x + col_x[0];
	int own_right = own_left + width - 1 - col_x[1];
	int own_top = new_y + col_y[0];
	int own_bottom = own_top + height - 1 - col_y[1];

	SDL_Rect own;
	own.x = own_left;
	own.y = own_top;
	own.w = own_right - own_left;
	own.h = own_bottom - own_top;


	//doesn't work
		/*int own_left2 = new_x;
		int own_right2 = own_left + width;
		int own_top2 = new_y;
		int own_bottom2 = own_top + height;*/

	/*std::vector<SDL_Rect> rects = GTileController::instance().GetColRects();
	for (int i = 0; i < rects.size(); i++)
	{
		int other_left = rects[i].x;
		int other_right = rects[i].x + rects[i].w;
		int other_top = rects[i].y;
		int other_bottom = rects[i].y + rects[i].h;

		if (own_bottom < other_top) continue;
		if (other_bottom < own_top) continue;
		if (own_right < other_left) continue;
		if (other_right < own_left) continue;

		return false;
	}*/

	std::vector<SDL_Point> points = GTileController::instance().GetColTris();
	if (!points.empty())
	{
		for (int i = 0; i <= points.size() - 3; i += 3)
		{
			if (TriangleCollision(points[i], points[i + 1], own) ||
				TriangleCollision(points[i + 1], points[i + 2], own) ||
				TriangleCollision(points[i + 2], points[i], own))
				return false;
		}
	}


	return true;




	/*std::vector<GEllipse> ellipses = GTileController::instance().GetColEllipses();

	for (int i = 0; i < ellipses.size(); i++)
	{
		if (Col_X < ellipses[i].x + ellipses[i].w &&
			Col_X + Col_Width > ellipses[i].x &&
			Col_Y < ellipses[i].y + ellipses[i].w &&
			Col_Y + Col_Height >  ellipses[i].y)
		{
			return true;
		}
	}*/
}

bool GYoshi::TriangleCollision(SDL_Point a, SDL_Point b, SDL_Rect rect)
{
	int m = (b.y - a.y) / (b.x - a.x);
	int c = a.y - (m * a.x);

	int top_intersection = 0;
	int bottom_intersection = 0;

	int toptrianglepoint;
	int bottomtrianglepoint;

	if (m > 0)
	{
		top_intersection = (m * rect.x + c);
		bottom_intersection = (m * (rect.x + rect.w) + c);
	}
	else
	{
		top_intersection = (m * (rect.x + rect.w) + c);
		bottom_intersection = (m * rect.x + c);
	}

	if (a.y < b.y)
	{
		toptrianglepoint = a.y;
		bottomtrianglepoint = b.y;
	}
	else
	{
		toptrianglepoint = b.y;
		bottomtrianglepoint = a.y;
	}

	int topoverlap = top_intersection > toptrianglepoint ? top_intersection : toptrianglepoint;
	int botoverlap = bottom_intersection < bottomtrianglepoint ? bottom_intersection : bottomtrianglepoint;

	return (topoverlap < botoverlap) && (!((botoverlap < rect.y) || (topoverlap > (rect.y + rect.h))));
}

bool GYoshi::PosValidEntity(GEntity* Entity, int NewX, int NewY)
{
	/*	if the entity is not the calling entity, nonexistent, dead or
	only colliding with map and Collides(...) is true	*/
	if (this != Entity && Entity != NULL /*&& Entity->Dead == false*/ &&
		Entity->flags ^ ENTITY_FLAG_MAPONLY &&
		//Entity->Collides(NewX + col_x[0], NewY + col_y[0], width - col_x[1] - 1, height - col_y[1] - 1) == true)
		Entity->Collides2(NewX + col_x[0], NewY + col_y[0], width - col_x[1] - 1, height - col_y[1] - 1, 0, 0, (collider_type)Entity->type, (collider_shape)Entity->shape) == true)
	{
		//Push to collision event list
		GEntityCol EntityCol;

		EntityCol.EntityA = this;
		EntityCol.EntityB = Entity;

		GEntityCol::EntityColList.push_back(EntityCol);

		//New pos is not valid
		return false;
	}

	//New pos is valid
	return true;
}




void GYoshi::OnRender(float extrap, SDL_Renderer* renderer)
{
	if (!render || texture->GetTexture() == nullptr || renderer == nullptr) return;

	int test = x + (int64_t)(fraction_x + speed_x_next * extrap);
	int test2 = y + (int64_t)(fraction_y + speed_y_next * extrap);

	//Includes extrapolation for MVC architecture
	texture->OnRenderMod(
		x + (int64_t)(fraction_x + speed_x_next * extrap) - GCamera::CameraControl.GetX(),
		y + (int64_t)(fraction_y + speed_y_next * extrap) - GCamera::CameraControl.GetY(),
		Anim_Control.CurrentFrameCol * texture->frame_width,
		(Anim_Control.CurrentFrameRow + Anim_Control.GetCurrentFrame()) * texture->frame_height,
		texture->frame_width,
		texture->frame_height,
		renderer,
		texture_angle,
		SDL_Point {
		0, 0
	},
		texture_flip,
			texture_blend,
			texture_red,
			texture_green,
			texture_blue,
			texture_alpha);

	//no extra- or interpolation, works just fine for me. just no extra frames to render
	/*texture->OnRenderMod(
		x - GCamera::CameraControl.GetX(),
		y - GCamera::CameraControl.GetY(),
		Anim_Control.CurrentFrameCol * texture->frame_width,
		(Anim_Control.CurrentFrameRow + Anim_Control.GetCurrentFrame()) * texture->frame_height,
		texture->frame_width,
		texture->frame_height,
		renderer,
		texture_angle,
		SDL_Point {
		0, 0
	},
		texture_flip,
			texture_blend,
			texture_red,
			texture_green,
			texture_blue,
			texture_alpha);*/




			//interpolation (only works if step_interval == 1)
			/*texture->OnRenderMod(
				x * extrap +  x_prev * (1.0f - extrap) - GCamera::CameraControl.GetX(),
				y * extrap + y_prev * (1.0f - extrap) - GCamera::CameraControl.GetY(),
				Anim_Control.CurrentFrameCol * texture->frame_width,
				(Anim_Control.CurrentFrameRow + Anim_Control.GetCurrentFrame()) * texture->frame_height,
				texture->frame_width,
				texture->frame_height,
				renderer,
				texture_angle,
				SDL_Point {
				0, 0
			},
				texture_flip,
					texture_blend,
					texture_red,
					texture_green,
					texture_blue,
					texture_alpha);*/


					//debug
					//sprite borders
					//this shows the actual location of the object, not extrapolation
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // the rect color (red)

	SDL_Rect rect;

	rect.x = x /*- texture->center.x*/ - GCamera::CameraControl.GetX();
	rect.y = y /*- texture->center.y*/ - GCamera::CameraControl.GetY();
	rect.w = texture->frame_width;
	rect.h = texture->frame_height;

	SDL_RenderDrawRect(renderer, &rect);

	//draw point(s) at x/y coordinate
	SDL_RenderDrawPoint(renderer, x - GCamera::CameraControl.GetX(), y - 1 - GCamera::CameraControl.GetY());
	SDL_RenderDrawPoint(renderer, x - GCamera::CameraControl.GetX(), y - GCamera::CameraControl.GetY());
	SDL_RenderDrawPoint(renderer, x - GCamera::CameraControl.GetX(), y + 1 - GCamera::CameraControl.GetY());
	SDL_RenderDrawPoint(renderer, x - 1 - GCamera::CameraControl.GetX(), y - GCamera::CameraControl.GetY());
	SDL_RenderDrawPoint(renderer, x + 1 - GCamera::CameraControl.GetX(), y - GCamera::CameraControl.GetY());
}

void GYoshi::OnCleanup()
{
	GEntity::OnCleanup();
}

bool GYoshi::OnCollision(GEntity* Entity)
{
	return true;
}

bool GYoshi::GetGrounded()
{
	return grounded;
}

float GYoshi::GetSpeedX()
{
	return speed_x;
}

float GYoshi::GetSpeedY()
{
	return speed_y;
}

float GYoshi::GetFracX()
{
	return fraction_x;
}

float GYoshi::GetFracY()
{
	return fraction_y;
}

int GYoshi::GetRenderX(float extrapolate)
{
	return x + (int64_t)(fraction_x + speed_x_next * extrapolate);
}

int GYoshi::GetRenderY(float extrapolate)
{
	return y + (int64_t)(fraction_y + speed_y_next * extrapolate);
}