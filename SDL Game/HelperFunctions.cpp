#include "stdafx.h"
#include "HelperFunctions.h"

std::string getBaseDir(const std::string &subDir)
{
#ifdef _WIN32
	const char PATH_SEP = '\\';
#else
	const char PATH_SEP = '/';
#endif
	std::string baseD(SDL_GetBasePath());
	return subDir.empty() ? baseD : baseD + subDir;
}

int random_int(const int &min, const int &max)
{
	return min + (rand() % (int)(max - min + 1));
}