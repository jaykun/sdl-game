#pragma once
#ifndef GSURFACE_H_
#define GSURFACE_H_
#include <iostream>
#include <string>
#include <SDL.h>
#include <SDL_image.h>

#include <SDL_mixer.h>
#include <SDL_ttf.h>

class GSurface
{
public:
	GSurface();

	static SDL_Surface *OnLoad(const std::string fileName);
	static bool OnDraw(SDL_Surface *surfDest, SDL_Surface *surfSrc, int x, int y);
	static bool OnDraw(SDL_Surface* surfDest, SDL_Surface* surfSrc, int x, int y, int x2, int y2, int w, int h);
	static bool Transparent(SDL_Surface* Surf_Dest, int R, int G, int B);
};






#endif