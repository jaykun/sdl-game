#pragma once
#ifndef GSTATE_H_
#define GSTATE_H_

#include "GPLayer.h"

class GState
{
public:
	GState();// = default;
	virtual ~GState();
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();
	virtual void SetPlayer(GPlayer* player);

protected:
	GPlayer* m_player;

private:


};

#endif