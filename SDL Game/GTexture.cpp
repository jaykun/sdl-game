#include "stdafx.h"
#include "GTexture.h"
#include <iostream>
#include <string>


GTexture::~GTexture()
{
	//Deallocate
	Free();
}

//Texture container
std::unordered_map<std::string, GTexture*> GTexture::texture_list{};

//GTexture retrieval method
GTexture *GTexture::GetTexture(const std::string name)
{
	auto texture = texture_list.find(name);

	if (texture == texture_list.end())
	{
		return nullptr;
	}
	else
	{
		return texture->second;
	}
}


bool GTexture::OnLoad(std::string path, SDL_Renderer *renderer)
{
	//Get rid of preexisting texture
	Free();

	std::string imgPath = SDL_GetBasePath();
	imgPath += path;

	//The final texture
	SDL_Texture* newTexture = nullptr;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(imgPath.c_str());

	if (loadedSurface == nullptr)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		//Load attributes
		GetInfo(path);

		//Color key image, most likely won't be used as .png with alpha exists
		//SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0xFF, 0xFF, 0xFF));

		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
		if (newTexture == nullptr)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}
		else
		{
			//Get image dimensions
			width = loadedSurface->w;
			height = loadedSurface->h;
		}

		//Set normal blendmode
		SDL_SetTextureBlendMode(newTexture, SDL_BLENDMODE_BLEND);
		blend_default = SDL_BLENDMODE_BLEND;

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);

		std::cout << "texture loaded succesfully" << std::endl;	
	}
	
	//Add this to texture container
	texture_list.insert({ path, this });

	//Return success
	texture = newTexture;
	return texture != nullptr;
}

bool GTexture::OnLoadText(std::string path, std::string textureText, int size, SDL_Color textColor, SDL_Renderer *renderer)
{
	//Get rid of preexisting texture
	Free();

	//Load temporary font
	std::string fullPath = SDL_GetBasePath();
	fullPath += path;

	TTF_Font *tempFont = TTF_OpenFont(fullPath.c_str(), size);

	if (tempFont == nullptr)
	{
		printf("Unable to find font! SDL_ttf Error: %s\n", TTF_GetError());
		return false;
	}

	//Render text to surface
	SDL_Surface* textSurface = TTF_RenderText_Solid(tempFont, textureText.c_str(), textColor);
	if (textSurface == nullptr)
	{
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
	}
	else
	{
		//Font no longer needed
		TTF_CloseFont(tempFont);

		//Create texture from surface
		texture = SDL_CreateTextureFromSurface(renderer, textSurface);

		if (texture == nullptr)
		{
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
		}
		else
		{
			//Get image dimensions
			width = textSurface->w;
			height = textSurface->h;
		}

		//Set normal blendmode
		SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
		blend_default = SDL_BLENDMODE_BLEND;

		//Get rid of surface
		SDL_FreeSurface(textSurface);
	}

	//Return success
	return texture != nullptr;
}

//Load an empty texture
bool GTexture::OnLoadBlank(int width, int height, SDL_TextureAccess access, SDL_Renderer *renderer)
{	
	texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, access, width, height);

	if (texture != nullptr)
	{
		this->width = width;
		this->height = height;
	}
	else
	{
		printf("Unable to create empty texture! SDL Error: %s\n", SDL_GetError());
	}
	
	return texture != nullptr;
}

//Add text to an existing texture
bool GTexture::AddText(SDL_Renderer *renderer, TTF_Font *font, int x, int y, std::string textureText, int size, SDL_Color textColor, float angle, SDL_RendererFlip flip)
{
	//Render text surface
	SDL_Surface* tempSurf = TTF_RenderText_Solid(font, textureText.c_str(), textColor);

	if (tempSurf == nullptr)
	{
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
		return false;
	}
	else
	{
		//Create texture from surface pixels
		SDL_Texture *tempText= SDL_CreateTextureFromSurface(renderer, tempSurf);

		if (tempText == nullptr)
		{
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
			return false;
		}

		//Set normal blendmode
		SDL_SetTextureBlendMode(tempText, SDL_BLENDMODE_BLEND);

		SDL_Rect destR;

		//Center text on requested x/y
		destR.x = x - tempSurf->w/2;
		destR.y = y - tempSurf->h/2;
		destR.w = tempSurf->w;
		destR.h = tempSurf->h;

		//Render to texture
		SDL_SetRenderTarget(renderer, texture);
		SDL_RenderCopyEx(renderer, tempText, nullptr, &destR, angle, nullptr, flip);
		SDL_SetRenderTarget(renderer, nullptr);

		//Get rid of temp surface and texture
		SDL_FreeSurface(tempSurf);
		SDL_DestroyTexture(tempText);
		return true;
	}
}

//Get collision boxes and information on how to divide spritesheet
bool GTexture::GetInfo(std::string metaPath)
{
	std::string fullPath = SDL_GetBasePath();
	std::size_t dot = metaPath.find("."); //problem with relative paths (include '.' and '..', make changes
	metaPath = metaPath.substr(0, dot) + ".meta";
	fullPath += metaPath;

	FILE* FileHandle = nullptr;

	errno_t err = fopen_s(&FileHandle, fullPath.c_str(), "r");

	if (err != 0)
	{
		std::cout << "Unable to find .meta-file: " << fullPath << std::endl;
		return false;
	}

	fscanf_s(FileHandle, "%d\n", &col_type);

	fscanf_s(FileHandle, "%d\n", &frame_width);
	fscanf_s(FileHandle, "%d\n", &frame_height);

	fscanf_s(FileHandle, "%d\n", &col_x);
	fscanf_s(FileHandle, "%d\n", &col_y);
	fscanf_s(FileHandle, "%d\n", &col_width);
	fscanf_s(FileHandle, "%d\n", &col_height);

	fscanf_s(FileHandle, "%d\n", &max_frames);

	std::cout << col_type << " " << frame_width << " " << frame_height << " " << col_x << " " << col_y << " " << col_width << " " << col_height << " " << max_frames << std::endl;

	return true;
}



void GTexture::Free()
{
	//Free texture if it exists
	if (texture != NULL)
	{
		SDL_DestroyTexture(texture);
		texture = NULL;
		width = 0;
		height = 0;
	}
}

void GTexture::SetColor(Uint8 red, Uint8 green, Uint8 blue)
{
	//Modulate texture rgb
	SDL_SetTextureColorMod(texture, red, green, blue);
	r_default = red;
	g_default = green;
	b_default = blue;	
}

void GTexture::SetBlendMode(SDL_BlendMode blending)
{
	//Set blending function
	SDL_SetTextureBlendMode(texture, blending);
	blend_default = blending;
}

void GTexture::SetAlpha(Uint8 alpha)
{
	//Modulate texture alpha
	SDL_SetTextureAlphaMod(texture, alpha);
	a_default = alpha;
}

//Simple render
void GTexture::OnRender(int x, int y, int x2, int y2, int w, int h, SDL_Renderer *renderer, float angle, SDL_Point point, SDL_RendererFlip flip)
{
	SDL_Rect destR;

	destR.x = x;
	destR.y = y;
	destR.w = w;
	destR.h = h;

	SDL_Rect srcR;

	srcR.x = x2;
	srcR.y = y2;
	srcR.w = w;
	srcR.h = h;

	//Render to screen
	SDL_RenderCopyEx(renderer, texture, &srcR, &destR, angle, &point, flip);
}

//Render function with additional options
void GTexture::OnRenderMod(int x, int y, int x2, int y2, int w, int h, SDL_Renderer *renderer, float angle, SDL_Point point, SDL_RendererFlip flip,
	SDL_BlendMode blending, Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	//Set modifiers
	SDL_SetTextureBlendMode(texture, blending);
	SDL_SetTextureColorMod(texture, r, g, b);
	SDL_SetTextureAlphaMod(texture, a);

	SDL_Rect destR;

	destR.x = x; // -center.x; //not sure what was ever the point here
	destR.y = y; // -center.y;
	destR.w = w;
	destR.h = h;

	SDL_Rect srcR;

	srcR.x = x2;
	srcR.y = y2;
	srcR.w = w;
	srcR.h = h;


	//Render to screen
	SDL_RenderCopyEx(renderer, texture, &srcR, &destR, angle, &point, flip);
	
	//Reset modifiers
	SDL_SetTextureBlendMode(texture, blend_default);
	SDL_SetTextureColorMod(texture, r_default, g_default, b_default);
	SDL_SetTextureAlphaMod(texture, a_default);
}

//Get SDL_Texture from GTexture
SDL_Texture* GTexture::GetTexture()
{
	return texture;
}

Uint32 GTexture::GetWidth() 
{
	return width;
}

Uint32 GTexture::GetHeight()
{
	return height;
}