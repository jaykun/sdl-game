#pragma once
#ifndef GEVENT_H_
#define GEVENT_H_

#include <SDL.h>

class GEvent {
public:
	GEvent();

	virtual ~GEvent();

	virtual void OnEvent(SDL_Event* Event);

	virtual void OnExit();

	virtual void OnKeyDown(SDL_Keycode sym/*, Uint16 mod*/, Uint16 repeat);

	virtual void OnKeyUp(SDL_Keycode sym/*, Uint16 mod*/);

	virtual void OnMouseMove(int mx, int my, int relx, int rely, bool Left, bool Right, bool Middle);

	virtual void OnLButtonDown(int mx, int my);

	virtual void OnLButtonUp(int mx, int my);

	virtual void OnRButtonDown(int mx, int my);

	virtual void OnRButtonUp(int mx, int my);

	virtual void OnMButtonDown(int mx, int my);

	virtual void OnMButtonUp(int mx, int my);

	virtual void OnMouseFocus();

	virtual void OnMouseBlur();

	virtual void OnInputFocus();

	virtual void OnInputBlur();

	virtual void OnWindowResized(int data1, int data2);

	virtual void OnJoyAxis(Uint8 which, Uint8 axis, Sint16 value);

	virtual void OnJoyBall(Uint8 which, Uint8 ball, Sint16 xrel, Sint16 yrel);

	virtual void OnJoyHat(Uint8 which, Uint8 hat, Uint8 button);

	virtual void OnJoyButtonDown(Uint8 which, Uint8 button);

	virtual void OnJoyButtonUp(Uint8 which, Uint8 button);

	virtual void OnControllerDeviceAdded(Uint32 type, int which);

	virtual void OnControllerDeviceRemoved(Uint32 type, int which);

	virtual void OnControllerButtonDown(Uint8 button, int which);

	virtual void OnControllerButtonUp(Uint8 button, int which);

	virtual void OnControllerAxisMotion(Uint8 axis, Sint16 value, int which);

	virtual void OnUser(Uint8 type, int code, void* data1, void* data2);
};

#endif