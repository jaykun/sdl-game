#include "stdafx.h"
#include "GEntity.h"
//#include <iostream>
//#include <string>


std::vector<GEntity*> GEntity::entityList;

GEntity::GEntity() 
{
	for (int i = 0; i < 3; i++)
	{
		col_x[i] = col_y[i] = 0;
	}

	entityList.push_back(this);
}

GEntity::~GEntity() 
{
	
}

bool GEntity::OnLoad(GTexture &texture_)
{
	this->texture = &texture_;

	this->width = texture_.frame_width;
	this->height = texture_.frame_height;

	this->col_x[0] = texture_.col_x;// -texture_.center.x;
	this->col_y[0] = texture_.col_y;// -texture_.center.y;
	this->col_x[1] = texture_.col_width;
	this->col_y[1] = texture_.col_height;

	this->Anim_Control.MaxFrames = texture_.max_frames;
	return true;
}


bool GEntity::SetCollisionMask(GTexture &texture_)
{
	this->width = texture_.frame_width;
	this->height = texture_.frame_height;

	this->col_x[0] = texture_.col_x;
	this->col_y[0] = texture_.col_y;
	this->col_x[1] = texture_.col_width;
	this->col_y[1] = texture_.col_height;

	return true;
}

void GEntity::OnInit()
{
	
}

void GEntity::OnLoop(float delta_time)
{

}

void GEntity::OnRender(float extrap, SDL_Renderer *renderer)
{
	if (texture == nullptr) return;
	if (texture->GetTexture() == nullptr || renderer == nullptr) return;

	//Draw to X Y with animation if present
	texture->OnRenderMod(
		x,
		y,
		Anim_Control.CurrentFrameCol * texture->frame_width,
		(Anim_Control.CurrentFrameRow + Anim_Control.GetCurrentFrame()) * texture->frame_height,
		texture->frame_width,
		texture->frame_height,
		renderer,
		texture_angle,
		SDL_Point {0, 0},
		texture_flip,
		texture_blend,
		texture_red,
		texture_green,
		texture_blue,
		texture_alpha);
}

void GEntity::OnCleanup() 
{
	//We could free the texture here, but other entities might be using it.
}

//What to do when colliding with an entity
bool GEntity::OnCollision(GEntity* Entity) 
{
	return false;
}

//Check own collision box against another, called by other entities in PosValidEntity, thus it should be in all entities
bool GEntity::Collides(int oX, int oY, int oW, int oH) 
{
	//calculate bounding boxes for objects. 1 = own, 2 = other
	int left1, left2;
	int right1, right2;
	int top1, top2;
	int bottom1, bottom2;

	int tX = (int)x + col_x[0];
	int tY = (int)y + col_y[0];

	left1 = tX;
	left2 = oX;

	right1 = left1 + width - 1 - col_x[1];
	right2 = oX + oW - 1;

	top1 = tY;
	top2 = oY;

	bottom1 = top1 + height - 1 - col_y[1];
	bottom2 = oY + oH - 1;

	//If objects cannot touch each other, return false
	if (bottom1 < top2) return false;
	if (top1 > bottom2) return false;

	if (right1 < left2) return false;
	if (left1 > right2) return false;

	//if above statements did not return false, the objects are colliding
	return true;
}

//Check own collision box against another, called by other entities in PosValidEntity, thus it should be in all entities
bool GEntity::Collides2(int other_x1, int other_y1, int other_x2, int other_y2, int other_x3, int other_y3, uint8_t other_type_, uint8_t other_shape_)
{
	if (other_type_ == COL_NONE)
		return false;

	//calculate bounding boxes for objects. 1 = own, 2 = other
	int left1, left2;
	int right1, right2;
	int top1, top2;
	int bottom1, bottom2;

	int own_x, own_y;

	int other_col_x[3];
	other_col_x[0] = other_x1;
	other_col_x[1] = other_x2;
	other_col_x[2] = other_x3;

	int other_col_y[3];
	other_col_y[0] = other_y1;
	other_col_y[1] = other_y2;
	other_col_y[2] = other_y3;

	int other_left;
	int other_width;
	int other_top;
	int other_height;

	switch (shape)
	{
	case COL_SHAPE_RECTANGLE:
		switch (other_shape_)
		{
		case COL_SHAPE_RECTANGLE:

			left1 = x + col_x[0];;
			left2 = other_x1;

			right1 = left1 + width - 1 - col_x[1];
			right2 = other_x1 + other_x2 - 1;

			top1 = y + col_y[0];
			top2 = other_y1;

			bottom1 = top1 + height - 1 - col_y[1];
			bottom2 = other_y1 + other_y2 - 1;

			//If objects cannot touch each other, return false
			if (bottom1 < top2) return false;
			if (top1 > bottom2) return false;

			if (right1 < left2) return false;
			if (left1 > right2) return false;

			//if above statements did not return false, the objects are colliding
			return true;
		case COL_SHAPE_ELLIPSE:
			return true;
		case COL_SHAPE_TRIANGLE:
			int own_left = x + col_x[0];
			int own_right = own_left + width - 1 - col_x[1];
			int own_top = y + col_y[0];
			int own_bottom = own_top + height - 1 - col_y[1];

			//needs better arguments from GPlayer to work
			if (TriangleCollision(other_x1, other_y1, other_x2, other_y2, own_left, own_top, own_right - own_left, own_bottom - own_top) ||
				TriangleCollision(other_x2, other_y2, other_x3, other_y3, own_left, own_top, own_right - own_left, own_bottom - own_top) ||
				TriangleCollision(other_x3, other_y3, other_x1, other_y1, own_left, own_top, own_right - own_left, own_bottom - own_top))
				return true;

			return false;
		}
	case COL_SHAPE_TRIANGLE:
		switch (other_shape_)
		{
		case COL_SHAPE_RECTANGLE:
			if (TriangleCollision(col_x[0], col_y[0], col_x[1], col_y[1], other_x1, other_y1, other_x2, other_y2) ||
				TriangleCollision(col_x[ 1], col_y[ 1], col_x[2], col_y[2], other_x1, other_y1, other_x2, other_y2) ||
				TriangleCollision(col_x[2], col_y[2], col_x[0], col_y[0], other_x1, other_y1, other_x2, other_y2))
				return true;
			
			return false;
		case COL_SHAPE_ELLIPSE:
			return false;
		case COL_SHAPE_TRIANGLE:
			return false;
		}
	}

}

int GEntity::GetX()
{
	return x;
}

int GEntity::GetY()
{
	return y;
}

bool GEntity::TriangleCollision(int tri_x1, int tri_y1, int tri_x2, int tri_y2, int rect_x, int rect_y, int rect_w, int rect_h)
{
	int m = (tri_y2 - tri_y1) / (tri_x2 - tri_x1);
	int c = tri_y1 - (m * tri_x1);

	int top_intersection = 0;
	int bottom_intersection = 0;

	int toptrianglepoint;
	int bottomtrianglepoint;

	if (m > 0)
	{
		top_intersection = (m * rect_x + c);
		bottom_intersection = (m * (rect_x + rect_w) + c);
	}
	else
	{
		top_intersection = (m * (rect_x + rect_w) + c);
		bottom_intersection = (m * rect_x + c);
	}

	if (tri_y1 < tri_y2)
	{
		toptrianglepoint = tri_y1;
		bottomtrianglepoint = tri_y2;
	}
	else
	{
		toptrianglepoint = tri_y2;
		bottomtrianglepoint = tri_y1;
	}

	int topoverlap = top_intersection > toptrianglepoint ? top_intersection : toptrianglepoint;
	int botoverlap = bottom_intersection < bottomtrianglepoint ? bottom_intersection : bottomtrianglepoint;

	return (topoverlap < botoverlap) && (!((botoverlap < rect_y) || (topoverlap > (rect_y + rect_h))));
}


std::vector<GEntity*> GWall::GWallList;

GWall::GWall()
{
	type = COL_SOLID;
	GWallList.push_back(this);
}