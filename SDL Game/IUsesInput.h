#pragma once
#ifndef IUSESINPUT_H_
#define IUSESINPUT_H_

class GInputPlayer;

class IUsesInput
{
public:
	virtual void SetInput(GInputPlayer* input_);
	GInputPlayer* GetInput();

	bool leftButton = 0;
	bool leftButtonPressed = 0;
	bool leftButtonReleased = 0;

	bool rightButton = 0;
	bool rightButtonPressed = 0;
	bool rightButtonReleased = 0;

	bool jumpButton = 0;
	bool jumpButtonPressed = 0;
	bool jumpButtonReleased = 0;

	bool runButton = 0;
	bool runButtonPressed = 0;
	bool runButtonReleased = 0;

protected:
	GInputPlayer *input;
};
#endif