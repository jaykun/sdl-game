#pragma once
#ifndef GMEMORYALLOCATOR_H_
#define GMEMORYALLOCATOR_H_

#include "PoolAllocator.h"
#include "StackAllocator.h"


class GYoshi;

struct GMemoryManager
{
	Allocator* main_allocator;
	StackAllocator* meta_allocator;

	StackAllocator* entity_allocator;
	size_t entity_allocator_size = 15 * 1024 * 1024;

	PoolAllocator* pool_gyoshi;
	size_t pool_gyoshi_size = 10;



};

#endif // !GMEMORYALLOCATOR_H_

