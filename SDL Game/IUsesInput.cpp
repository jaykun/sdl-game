#include "stdafx.h"
#include "IUsesInput.h"

void IUsesInput::SetInput(GInput* input_)
{
	input = input_;
}

GInput* IUsesInput::GetInput()
{
	return input;
}