#pragma once
#ifndef GBUFFERREADER_H_
#define GBUFFERREADER_H_

#include "GInputPlayer.h"
#include "GInputSequence.h"
#include "GInputRepeat.h"
#include "GInputHold.h"

class GBufferReader
{
public:
	GBufferReader();
	virtual ~GBufferReader() = default;
	GInput* GetInput();
	void SetInput(GInput *input);
	virtual void ReadInput();
	static void UpdateTime(uint64_t now);
	uint32_t GetState();
	uint32_t GetStatePressed();
	uint32_t GetStateReleased();
	
protected:
	GInput* input;
	static uint64_t current_time;

	uint32_t state;
	uint32_t prev_state;
	uint32_t state_pressed;
	uint32_t state_released;
};





#endif