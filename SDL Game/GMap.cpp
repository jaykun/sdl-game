#include "stdafx.h"
#include "GMap.h"
#include <iostream>
#include <fstream>
#include <string>

GMap::GMap() 
{
	Surf_Tileset = nullptr;
}

bool GMap::OnLoad(const char* File) 
{
	TileList.clear();

	//Attempt to open file
	FILE* FileHandle = nullptr;
		
	errno_t err = fopen_s(&FileHandle, File, "r");

	if (err != 0) 
	{
		return false;
	}

	//Create tiles and insert to tilevector according to file
	for (int Y = 0; Y < MAP_HEIGHT; Y++)
	{
		for (int X = 0; X < MAP_WIDTH; X++) 
		{
			GTile tempTile;

			fscanf_s(FileHandle, "%d:%d ", &tempTile.TileID, &tempTile.TypeID);
			TileList.push_back(tempTile);
		}
		fscanf_s(FileHandle, "\n");
	}
	fclose(FileHandle);

	return true;
}

void GMap::OnRender(SDL_Renderer *renderer, int MapX, int MapY)
{
	//Return if no tileset present
	if (text == nullptr) return;

	//Get tileset width and height in tiles
	int TilesetWidth = text->GetWidth() / TILE_SIZE;
	int TilesetHeight = text->GetHeight() / TILE_SIZE;

	int ID = 0;

	//Render tiles from top-left to bottom-right
	for (int Y = 0; Y < MAP_HEIGHT; Y++)
	{
		for (int X = 0; X < MAP_WIDTH; X++)
		{
			if (TileList[ID].TypeID == TILE_TYPE_NONE)
			{
				ID++;
				continue;
			}

			int tX = MapX + (X * TILE_SIZE);
			int tY = MapY + (Y * TILE_SIZE);

			int TilesetX = (TileList[ID].TileID % TilesetWidth) * TILE_SIZE;
			int TilesetY = (TileList[ID].TileID / TilesetWidth) * TILE_SIZE;

			text->OnRender(tX, tY, TilesetX, TilesetY, TILE_SIZE, TILE_SIZE, renderer);

			ID++;
		}
	}
}

//Get tile at XY
GTile* GMap::GetTile(int X, int Y) 
{
	int ID = 0;

	ID = X / TILE_SIZE;
	ID = ID + (MAP_WIDTH * (Y / TILE_SIZE));

	if (ID < 0 || ID >= TileList.size()) 
	{
		return NULL;
	}

	return &TileList[ID];
}

