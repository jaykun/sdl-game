#include "stdafx.h"
#include "GSurface.h"

#include <iostream>
#include <string>

GSurface::GSurface()
{

}


SDL_Surface *GSurface::OnLoad(const std::string fileName)
{
	SDL_Surface* surf_temp = nullptr;

	std::string fileLocation(SDL_GetBasePath());
	fileLocation += fileName;

	surf_temp = IMG_Load(fileLocation.c_str());

	if (surf_temp == nullptr)
	{
		return nullptr;
	}

	return surf_temp;
}

//Draw whole image
bool GSurface::OnDraw(SDL_Surface *surfDest, SDL_Surface *surfSrc, int x, int y)
{
	if (surfDest == nullptr || surfSrc == nullptr)
	{
		return false;
	}

	SDL_Rect destR;
	destR.x = x;
	destR.y = y;

	SDL_BlitSurface(surfSrc, NULL, surfDest, &destR);

	return true;
}

//Draw part of image
bool GSurface::OnDraw(SDL_Surface *surfDest, SDL_Surface *surfSrc, int x, int y, int x2, int y2, int w, int h) 
{
	if (surfDest == nullptr || surfSrc == nullptr) 
	{
		return false;
	}

	SDL_Rect destR;

	destR.x = x;
	destR.y = y;

	SDL_Rect srcR;

	srcR.x = x2;
	srcR.y = y2;
	srcR.w = w;
	srcR.h = h;

	SDL_BlitSurface(surfSrc, &srcR, surfDest, &destR);

	return true;
}

//Set transparency according to colour
bool GSurface::Transparent(SDL_Surface* Surf_Dest, int r, int g, int b)
{
	if (Surf_Dest == NULL)
	{
		return false;
	}

	SDL_SetColorKey(Surf_Dest, SDL_TRUE, SDL_MapRGB(Surf_Dest->format, r, g, b));

	return true;
}