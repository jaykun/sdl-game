#include "stdafx.h"
#include "GInput.h"

GInput::~GInput()
{

}

//void GInput::OnInput(uint64_t current_time)
//{
//
//}

void GInput::FlushBufferBefore(uint64_t time)
{
	//this is a ring buffer https://gameprogrammingpatterns.com/event-queue.html
	//it uses uint8 overflow as a mechanism
	//increment buffer_start as long as timestamp is smaller than time or we reach the end of array
	for (; timestamp[buffer_start] <= time && buffer_start != buffer_index; buffer_start++);
}

uint32_t* GInput::GetStateBuffer()
{
	return state;
}

uint64_t* GInput::GetTimestampBuffer()
{
	return timestamp;
}

uint8_t GInput::GetBufferStart()
{
	return buffer_start;
}

uint8_t GInput::GetBufferIndex()
{
	return buffer_index;
}

