#pragma once
#ifndef GBUFFERREADERSPECIAL_H_
#define GBUFFERREADERSPECIAL_H_

#include "GBufferReader.h"

class GBufferReaderSpecial: public GBufferReader
{
public:
	GBufferReaderSpecial();
	~GBufferReaderSpecial() = default;
	virtual void ReadInput();
	uint32_t GetStateSpecial();
private:
	uint32_t state_special = 0;
	GInputSequence sequence[16];
	GInputRepeat repeat[8];
	GInputHold hold[8];
};





#endif