#include "stdafx.h"
#include "GTime.h"

GTime::GTime()
{
	m_ui64Resolution = SDL_GetPerformanceFrequency();
	m_ui64LastRealTime = SDL_GetPerformanceCounter();
}

void GTime::Update() 
{
	uint64_t ui64RealTimeNow = SDL_GetPerformanceCounter();
	uint64_t ui64DeltaTime = ui64RealTimeNow - m_ui64LastRealTime;
	m_ui64LastRealTime = ui64RealTimeNow;

	UpdateBy(ui64DeltaTime);
}

void GTime::UpdateBy(uint64_t _ui64Ticks) {
	m_ui64LastTime = m_ui64CurTime;
	m_ui64CurTime += _ui64Ticks;

	// 1 s us = 1 * 1000000 s
	// x s us = x * 1000000 s
	// Hz = ticks / s
	// t secs = ticks / Hz
	// t micros = (ticks / Hz) * (1000000.0 / 1.0)    
	// <=> (ticks * 1000000.0) / (Hz)

	uint64_t ui64LastMicros = m_ui64CurMicros;
	m_ui64CurMicros = m_ui64CurTime * 1000000ULL / m_ui64Resolution;
	m_ui64DeltaMicros = m_ui64CurMicros - ui64LastMicros;
	m_fDeltaSecs = static_cast<double>(1.0 / 1000000.0) * m_ui64DeltaMicros;
}

uint64_t GTime::CurTime() const
{
	return m_ui64CurTime;
}

uint64_t GTime::CurMicros() const
{
	return m_ui64CurMicros;
}

uint64_t GTime::DeltaMicros() const
{
	return m_ui64DeltaMicros;
}

double GTime::DeltaSecs() const
{
	return m_fDeltaSecs;
}