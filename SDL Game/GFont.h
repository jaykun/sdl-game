#pragma once
#ifndef GFONT_H_
#define GFONT_H_

#include <iostream>
#include <unordered_map>
#include <SDL_ttf.h>

//Attempt at a class storing TTF_Fonts
//Issue is that same font with different size needs another TTF_Font
//Really unflexible, this needs work
class GFont
{
public:	
	GFont();
	~GFont();	
	TTF_Font* GetFont(std::string name, int size);
	void OnCleanup();
private:
	

	static std::unordered_map<std::string, TTF_Font*> font_list;
};







#endif
