#include "stdafx.h"
#include <iostream>
#include <string>
#include "GameMaster.h"
#include "GCamera.h"
#include "HelperFunctions.h"
#include "GBufferReader.h"
#include "GInputYoshiAI.h"

void GameMaster::CalculateDelta()
{	
	//ui64_resolution is count per second of SDL_GetPerformanceCounter(). All time needs be divided by this before use.
	current_time = SDL_GetPerformanceCounter() / ui64_resolution;
	
	uint64_t temp = current_time - last_time;

	//delta_time since previous Onloop(), used by OnInput entities
	u_time_accumulator += temp;

	//current_time and last_time are in microseconds, divide by one million to get seconds
	//multiply by 60 so 16,667 milliseconds gets a value of 1.	
	delta_time = (temp / 1000000.0f) * 60;
	last_time = current_time;

	if (old_time + 1000000ULL < current_time)
	{
		old_time = current_time;
		//render FPS
		fps = frames;
		frames = 0;
		//loop FPS
		loop_fps = loop_frames;
		loop_frames = 0;

		//Update debug text accordingly
		std::string test = "render_fps: " + std::to_string(fps) + " loop_fps: " + std::to_string(loop_fps) + " time: " + std::to_string(current_time / 1000000.0f);
		SDL_Color c = { 255, 0, 255 };
		text_font.OnLoadText("lazy.ttf", test, 20, c, renderer);
	}
	
	frames++;
}


void GameMaster::OnInput()
{
	//Keyboard is same for all inputs, update it here
	GInputPlayer::UpdateKB();

	//Get all input KB or GP
	for (int i = 0; i < MAX_INPUTS; ++i)
	{
		inputs[i].OnInput(current_time);
	}

	//moved inside loop
	//enemyAI->OnInput(current_time);
}

void GameMaster::OnLoop()
{
	//Add dt to time accumulator
	time_accumulator += delta_time;

	//If time accumulator is greater than step_interval, run one loop of the game
	//step_interval 1 for 60 fps (1/60*60)
	//				2 for 30fps (1/60*30)
	while (time_accumulator >= step_interval)
	{
		//Used by inputs, set current_time to first OnCalculateDelta() since previous loop
		current_time -= u_time_accumulator;
		u_time_accumulator = 0;

		//add frame time to current_time
		//this is so we can see which inputs were made before the frame we are currently processing
		//for time_acc 2.9, this will be done 2 times so no need to overengineer this
		current_time += frame_time_microseconds;

		//Update current_time for all GBufferReaders. Read input until timestamp
		GBufferReader::UpdateTime(current_time);

		//used for fps other than 60
		//cover the "global" time_factor with a local one that's multiplied by delta
		//for 60fps there will be no change, for 30fps it's doubled
		float temp = time_factor;
		float time_factor = temp * step_interval;		

		//Actual updating of ingame entities etc
		if (loop)
		{
			//testing
			enemyAI->OnInput(current_time, time_factor);

			//Go through the OnLoop() of all entities
			for (int i = 0; i < GEntity::entityList.size(); i++)
			{
				//can you '!' nullptrs? weird.
				if (!GEntity::entityList[i]) continue;

				GEntity::entityList[i]->OnLoop(time_factor);
			}

			//Update camera location
			//GCamera::CameraControl.OnLoop(step_interval, time_factor);			

			//Go through all collisions that were created in previous for loop
			for (int i = 0; i < GEntityCol::EntityColList.size(); i++)
			{
				GEntity* EntityA = GEntityCol::EntityColList[i].EntityA;
				GEntity* EntityB = GEntityCol::EntityColList[i].EntityB;

				if (EntityA == nullptr || EntityB == nullptr) continue;

				if (EntityA->OnCollision(EntityB))
				{
					EntityB->OnCollision(EntityA);
				}
			}

			//Clear all collisions
			GEntityCol::EntityColList.clear();

			//To be used with FPS counter
			loop_frames++;
		}

		//Ingame menu, decoupled from rest of the loop so it can control it
		ingame_menu.OnLoop();

		//For inputs
		//flush buffers before current_time
		for (int i = 0; i < MAX_INPUTS; ++i)
		{			
			inputs[i].FlushBufferBefore(current_time);
		}

		enemyAI->FlushBufferBefore(current_time);

		//Remove one step_interval from time_accumulator so we don't get stuck here indefinitely
		time_accumulator -= step_interval;
	}

	//used if we want to render interpolation between previous and current frame
	//doesn't seem to really be necessary...
	alpha = time_accumulator / step_interval;
}

void GameMaster::SetLoop(bool value)
{
	loop = value;
}

