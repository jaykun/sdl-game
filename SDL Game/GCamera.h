#pragma once
#ifndef GCAMERA_H_
#define GCAMERA_H_

#include <SDL.h>
#include "GPlayer.h"
#include "Define.h"

enum 
{
	TARGET_MODE_NORMAL = 0,
	TARGET_MODE_CENTER
};

class GCamera 
{
public:
	GCamera();

	void OnInit(GPlayer &player_);
	void OnLoop(float delta_time, float extrapolation, float time_factor);
	void OnMove(int MoveX, int MoveY);
	void OnRender(SDL_Renderer *renderer);	
	void OnLoop2(float delta_real, float delta_time);	//works - not accurate - not in use
	void Shake(float delta_real, int x_, int y_, int count);

	int GetX();
	int GetY();

	void SetPos(int x_, int y_);
	void SetTarget(int &x_, int &y_);

	int TargetMode;
	static GCamera CameraControl;

	bool loop = true;
	bool render = true;	

	
private:
	//debug
	bool camera_moved_x = false;
	int frames_used = 0;
	float time_acc_counter = 0;

	//basic
	int x;
	int y;

	float delta_time = 1;

	GPlayer* player;

	//int* target_object_x;
	//int* target_object_y;
	

	//horizontal
	float frames_to_target_x = 60.0f; //60 frames = 1 second
	float frames_processed_x = 0.0f;

	int center_x = 0;
	int middle_x = WWIDTH / 2;
	int center_offset_x = 128;
	int border_x = 64;

	float distance_moved_x = 0;

	float cam_speed_x = 10.0f;
	int cam_dir_x = 1;

	float start_pos_x = 0;
	float target_pos_x = 0;
	float function_value_x = 0.51;


	//vertical
	float frames_to_target_y = 60.0f;
	float frames_processed_y = 0.0f;

	int center_y = 0;
	int middle_y = WHEIGHT / 2;
	//center_offset_y would go here, not needed for now
	int border_y = 128;

	float distance_moved_y = 0;

	float cam_speed_y = 8.0f;
	int cam_dir_y = 1;
	
	float start_pos_y = 0;
	float target_pos_y = 0;
	float function_value_y = 0.51;


	//shake
	int shake_seq = 0;
	int shakecount = 0;
	float shake_threshold = 0;
	int xshake = 0;
	int yshake = 0;

	int xshake_amount = 0;
	int yshake_amount = 0;
};
#endif
