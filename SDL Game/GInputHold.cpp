#include "stdafx.h"
#include "GInputHold.h"

#include "stdafx.h"
#include "GInputRepeat.h"

void GInputHold::SetTimelimit(uint64_t min_ms)
{
	this->min_ms = min_ms;
}

void GInputHold::SetButton(uint32_t button)
{
	this->button = button;
}

void GInputHold::UpdateTime(uint64_t time_)
{
	time = time_;
}

bool GInputHold::Handle(uint32_t state, uint32_t state_pressed, uint32_t state_released)
{
	//there could be different 'hold types' determined by a member variable
	
	if (state_pressed & (1U << button))
	{
		//std::cout << "pressed " << time << std::endl;
		min_time = time + min_ms;
	}
	else if (state_released & (1U << button))
	{
		//std::cout << "released" << std::endl;
		min_time = UINT64_MAX;
	}
	else if (time > min_time)
	{
		//std::cout << "holding " << time << std::endl;
		return true;
	}

	return false;
}

void GInputHold::Active(bool val)
{
	this->active = val;
}

bool GInputHold::Active()
{
	return active;
}

