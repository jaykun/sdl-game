#include "stdafx.h"
#include "GFont.h"

std::unordered_map<std::string, TTF_Font*> GFont::font_list{};

TTF_Font* GFont::GetFont(std::string name, int size)
{
	auto font  = font_list.find(name);

	if (font == font_list.end())
	{
		std::string fullPath = SDL_GetBasePath();
		fullPath += name;

		TTF_Font *tempFont = TTF_OpenFont(fullPath.c_str(), size);

		if (tempFont == nullptr)
		{
			printf("Unable to find font! SDL_ttf Error: %s\n", TTF_GetError());
			return false;
		}

		font_list.insert({ name, tempFont });

		return tempFont;
	}
	else
	{
		return font->second;
	}




}