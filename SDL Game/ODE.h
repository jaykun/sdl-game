#pragma once
#ifndef GODE_H_
#define GODE_H

#include <vector>

class ODE
{
public:
	ODE(int numeq_, double s_);
	virtual ~ODE() = default;

	//Number of dependent variables
	int GetEqnum();
	void SetEqnum(int eqnum__);

	//Indepedent variable
	double GetS();
	void SetS(int s_);

	//get pointer to dependent variable array 
	virtual double *GetQ() = 0;

	//set select dependent variable
	virtual void SetQ(int index, double value) = 0;

	//calculates right hand side
	virtual double* GetRHS(double s, double ds, double q[], double dq[], double qscale) = 0;
private:
	//number of dependent variables
	int numeq;

	//indepedent variable (e.g. time)
	double s;
};

//The RK4 function, should take all inheritants of class ODE
void RK4(ODE &ode, double ds);

class ODEBasic : public ODE
{
public:
	ODEBasic(int eqnum_, double s_, double accel, double velocity, double pos);

	virtual double *GetQ();

	virtual void SetQ(int index, double value);

	virtual double* GetRHS(double s, double ds, double q[], double dq[], double qscale);

private:
	double q[2];

	//Values used in RHS calculation
	double accel;
	double velocity;
	double pos;
};


class ODESpring : public ODE
{
public:
	ODESpring(int eqnum_, double s_, double mass, double mu, double k, double x0);

	virtual double *GetQ();

	virtual void SetQ(int index, double value);

	virtual double* GetRHS(double s, double ds, double q[], double dq[], double qscale);

private:
	double q[2];

	//Values used in RHS calculation
	double mass;
	double mu;
	double k;
	double x0;
};





#endif
