#include "stdafx.h"
#include "GameMaster.h"
#include "GCamera.h"
#include "GPLayer.h"
#include "GInputPlayer.h"
#include <iostream>
#include <string>

void GameMaster::OnEvent(SDL_Event *event)
{
	if (event->type == SDL_QUIT)
	{
		running = false;
	}
}

void GameMaster::OnExit()
{
	running = false;
}

void GameMaster::OnControllerDeviceAdded(Uint32 type, int which)
{
	if (which < MAX_CONTROLLERS)
	{
		if (gameControllers[which] == nullptr)
		{
			gameControllers[which] = SDL_GameControllerOpen(which);

			//If a GInput is without gamepad, add the newly inserted gamepad to that ginput
			for (int i = 0; i < MAX_INPUTS; ++i)
			{
				if (inputs[i].GetGamePad() == nullptr)
				{
					inputs[i].SetGamepad(gameControllers[which]);
					break;
				}
			}

			std::cout << "controller added to slot" << which << std::endl;
		}		
	}
	else
	{
		std::cout << "can't add controller: max amount reached" << std::endl;
	}

}

void GameMaster::OnControllerDeviceRemoved(Uint32 type, int which)
{
	std::cout << "controller removed from slot" << which << std::endl;
	SDL_GameControllerClose(gameControllers[which]);

	//If gamepad is associated with a ginput, remove it + haptic device from ginput
	for (int i = 0; i < MAX_INPUTS; ++i)
	{
		if (inputs[i].GetGamePad() == gameControllers[which])
		{
			inputs[i].SetGamepad(nullptr);
			inputs[i].RemoveHaptic();
		}
	}

	gameControllers[which] = nullptr;
}