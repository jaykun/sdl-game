#include "stdafx.h"
#include "GInputRepeat.h"

void GInputRepeat::SetTimelimit(uint32_t max_ms)
{
	this->max_ms = max_ms;
}

void GInputRepeat::SetButton(uint32_t button)
{
	this->button = button;
}

bool GInputRepeat::UpdateTime(uint64_t time_)
{
	time = time_;

	if (time > max_time)
	{
		//std::cout << "inactive" << std::endl;
		return false;
	}
	//std::cout << "active" << std::endl;
	return true;
}

void GInputRepeat::Handle(uint32_t state_pressed)
{
	if (state_pressed & (1U << button))
	{		
		max_time = time + max_ms;
	}
}

void GInputRepeat::Active(bool val)
{
	this->active = val;
}

bool GInputRepeat::Active()
{
	return active;
}