#pragma once
#ifndef GPLAYERSTATEAIRWALK_H_
#define GPLAYERSTATEAIRWALK_H_

#include "GState.h"

class GPlayerStateAirWalk : public GState
{
public:
	GPlayerStateAirWalk(GPlayer* player);// = default;
	virtual ~GPlayerStateAirWalk() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif