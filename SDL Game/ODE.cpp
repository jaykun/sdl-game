#include "stdafx.h"
#include "ODE.h"


ODE::ODE(int numeq_, double s_) : numeq(numeq_), s(s_)
{

}

int ODE::GetEqnum()
{
	return numeq;
}

void ODE::SetEqnum(int numeq_)
{
	numeq = numeq_;
}

double ODE::GetS()
{
	return s;
}

void ODE::SetS(int s_)
{
	this->s = s_;
}


ODEBasic::ODEBasic(int eqnum_, double s_, double accel_, double velocity_, double pos_) : ODE(eqnum_, s_), accel(accel_), velocity(velocity_), pos(pos_)
{
	q[0] = pos;
	q[1] = velocity;
}

double *ODEBasic::GetQ()
{
	return q;
}

void ODEBasic::SetQ(int index, double value)
{
	q[index] = value;
}

//note, accel should be accel*deltatime in parameter
double* ODEBasic::GetRHS(double s, double ds, double q[], double dq[], double qscale)
{
	//Value to be returned
	double *rhs = new double[2];

	//Intermediate values
	double newq[2];

	for (int i = 0; i < 2; i++)
	{
		newq[i] = q[i] + qscale*dq[i];
	}

	//Calculate RHS
	rhs[0] = newq[0] + (newq[1] + accel*ds / 2) * ds;
	rhs[1] = newq[1] + accel*ds;
	return rhs;
}




ODESpring::ODESpring(int eqnum_, double s_, double mass_, double mu_, double k_, double x0_) : ODE(eqnum_, s_), mass(mass_), mu(mu_), k(k_), x0(x0_)
{
	//Set dependent variables

	//speed
	q[0] = 0;

	//initial spring deflection
	q[1] = x0;
}

double *ODESpring::GetQ()
{
	return q;
}

void ODESpring::SetQ(int index, double value)
{
	q[index] = value;
}

//Used to calculate new values
double* ODESpring::GetRHS(double s, double ds, double q[], double dq[], double qscale)
{
	//Value to be returned
	double *rhs = new double[2];

	//Intermediate values
	double newq[2];

	for (int i = 0; i < 2; i++)
	{
		newq[i] = q[i] + qscale*dq[i];
	}

	//Calculate RHS
	rhs[0] = -ds * (mu * newq[0] + k * newq[1]) / mass;
	rhs[1] = ds * newq[0];

	return rhs;
}


void RK4(ODE &ode, double ds)
{
	//Get number of ODEs to integrate
	int numeq = ode.GetEqnum();

	//Get independent value
	double s = ode.GetS();

	//Get dependent values
	double *q = ode.GetQ();

	//Compute the 4 RK steps
	double *dq1 = ode.GetRHS(s, ds, q, q, 0.0);
	double *dq2 = ode.GetRHS(s + 0.5 * ds, ds, q, dq1, 0.5);
	double *dq3 = ode.GetRHS(s + 0.5 * ds, ds, q, dq2, 0.5);
	double *dq4 = ode.GetRHS(s, ds, q, dq3, 1.0);

	//Update independent value
	ode.SetS(s + ds);

	//Update dependent values
	for (int j = 0; j < numeq; j++)
	{
		//iterate array pointers
		q += j;
		dq1 += j;
		dq2 += j;
		dq3 += j;
		dq4 += j;

		//calculate new value from the 4 steps
		*q = *q + (*dq1 + 2.0*(*dq2) + 2.0*(*dq3) + (*dq4) / 6.0);

		//set new value
		ode.SetQ(j, *q);
	}

	//Delete the dynamically allocated dq's
	delete dq1;
	delete dq2;
	delete dq3;
	delete dq4;

	return;
}