#pragma once
#ifndef GPLAYERSTATEAIRRUN_H_
#define GPLAYERSTATEAIRRUN_H_

#include "GState.h"

class GPlayerStateAirRun : public GState
{
public:
	GPlayerStateAirRun(GPlayer* player);// = default;
	virtual ~GPlayerStateAirRun() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif