#include "stdafx.h"

#include <iostream>
#include <string>

#include "GameMaster.h"
#include "GSurface.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
//#include <SDL_ttf.h>
#include "Define.h"
#include "GArea.h"
#include "GInputPlayer.h"
#include "GCamera.h"
#include "GInputYoshiAI.h"

bool GameMaster::OnInit()
{
	//Basic setup
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) 
	{
		return false;
	}

	window = SDL_CreateWindow(
		"SDL Window",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		WWIDTH,
		WHEIGHT,
		SDL_WINDOW_RESIZABLE);

	if (window == nullptr)
	{
		return false;
	}
	
	windowSurface = SDL_GetWindowSurface(window);

	//renderer test
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (renderer == nullptr)
	{
		printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
		return false;
	}
	else
	{
		//Initialize renderer color
		SDL_SetRenderDrawColor(renderer, 0xFF, 0x0, 0x0, 0xFF);

		//The view stretches with the window, complete with letterboxing
		SDL_RenderSetLogicalSize(renderer, WWIDTH, WHEIGHT);

		//Initialize PNG loading
		int imgFlags = IMG_INIT_PNG;
		if (!(IMG_Init(imgFlags) & imgFlags))
		{
			printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
			return false;
		}

		//Initialize sound
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		{
			printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
			return false;
		}

		//Initialize SDL_ttf
		if (TTF_Init() == -1)
		{
			printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
			return false;
		}

		//Check for joysticks
		int MaxJoysticks = SDL_NumJoysticks();
		int ControllerIndex = 0;
		for (int JoystickIndex = 0; JoystickIndex < MaxJoysticks; ++JoystickIndex)
		{
			gameControllers[ControllerIndex] = nullptr;

			if (!SDL_IsGameController(JoystickIndex))
			{
				printf("Joystick %i is not supported by the game controller interface!\n", JoystickIndex);
				continue;
			}
			else if (ControllerIndex >= MAX_CONTROLLERS)
			{
				break;
			}
			gameControllers[ControllerIndex] = SDL_GameControllerOpen(JoystickIndex);

			//set first 4 detected gamepads as designated controllers
			for (int i = 0; i < MAX_INPUTS; ++i)
			{
				if (inputs[i].GetGamePad() == nullptr)
				{
					inputs[i].SetGamepad(gameControllers[ControllerIndex]);
					break;
				}
			}
			ControllerIndex++;
		}
	}

	//Memory allocation

	/* We'll need to allocate:
		- textures
		- AIs
		- sounds
		- Level geometry
		- Game objects (GEntities for now)
	
	Pool allocator seems like a good idea for anything with more than 10 instances
	
	*/

	void* memblock = malloc(mem_size);
	mem_allocator.init(mem_size, memblock);
	mem_mgr.main_allocator = &mem_allocator;

	size_t meta_size = 16 * 1024 * 1024;
	void* meta_block = malloc(meta_size);
	meta_allocator.init(meta_size, meta_block);
	mem_mgr.meta_allocator = &meta_allocator;

	size_t test_size = 10 * 1024 * 1024;
	void* testblock = mem_allocator.allocate(test_size, 2);
	test_allocator.init(test_size, testblock);

	//Tilemaps

	std::string tilemappath =  SDL_GetBasePath();//"C:/Users/Jerry/source/repos/sdl-game/x64/Debug/"; //SDL_GetBasePath();
	//tilemappath += "tiles.json";	
	std::cout << "relative path: " <<  fs::relative(tilemappath) << std::endl;
	std::cout << "current path: " << fs::current_path() << std::endl;
	fs::path path(tilemappath, fs::path::format::auto_format);
	std::cout << "test path: " << path.relative_path().u8string() << std::endl;
	GTileController::instance().initialize(fs::relative(tilemappath));
	GTileController::instance().parseMap("sdlgamemap.json", renderer);

	GTileController::instance().scanObjectLayer(mem_mgr);

	//Timing for OnCalculateDelta() and OnLoop()
	ui64_resolution = SDL_GetPerformanceFrequency() / 1000000ULL;
	last_time = SDL_GetPerformanceCounter() / ui64_resolution;
	frames_per_second = 60;
	frame_time_microseconds = uint64_t(std::roundf(1000000.0f / frames_per_second));
	step_interval = 60 / frames_per_second;

	std::cout << "perf_freq = " << ui64_resolution << std::endl;
	std::cout << "frame_time us = " << frame_time_microseconds << std::endl;
	std::cout << "step_interval = " << frame_time_microseconds << std::endl;
	

	//Set up player resources
	jumpsound.OnLoad("low.wav");
	text.OnLoad("yoshi.png", renderer);

	GYoshi* Yoshi = GTileController::instance().GetMonsters()[0];
	enemyAI = new GInputYoshiAI(Yoshi);
	GInput* ai = enemyAI;
	Yoshi->OnInit(enemyAI);
	//GEntity::entityList.push_back(Yoshi);

	//Set player location
	Player.x = 300;
	Player.y = 300;

	Player.OnInit(step_interval);

	//Testing TTF
	SDL_Color c = { 255, 0, 255 };
	text_font.OnLoadText("lazy.ttf", "testing text", 20, c, renderer);

	//pass a font to use with menu to GIngamemenu
	std::string fullPath = SDL_GetBasePath();
	fullPath += "lazy.ttf";
	testfont = TTF_OpenFont(fullPath.c_str(), 30);	

	//Add objects to the list of entities to go through in onloop()
	//GEntity::entityList.push_back(&Player);

	//Keep camera centered at player location
	GCamera::CameraControl.OnInit(Player);

	//Init ingame menu
	ingame_menu.OnInit(this, testfont);

	//Init all entities
	for (int i = 0; i < GEntity::entityList.size(); i++)
	{
		if (!GEntity::entityList[i]) continue;

		GEntity::entityList[i]->OnInit();
	}

	inputs[1].SetKeyboard(false);
	inputs[2].SetKeyboard(false);
	inputs[3].SetKeyboard(false);
	
	return true;
}

