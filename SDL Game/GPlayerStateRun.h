#pragma once
#ifndef GPLAYERSTATERUN_H_
#define GPLAYERSTATERUN_H_

#include "GPlayerStateGround.h"
#include "GPlayerStateWalk.h"

class GPlayerStateRun : public GState
{
public:
	GPlayerStateRun(GPlayer* player);// = default;
	virtual ~GPlayerStateRun() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif