#pragma once
#ifndef GYOSHI_H_
#define GYOSHI_H_

#include "GEntity.h"
#include "GSound.h"
#include "GBufferReader.h"

//forward declaration needed, actual include in .cpp file 
class GInputYoshiAI;

class GYoshi : public GEntity
{
public:
	GYoshi() = default;

	bool OnLoad(GTexture& texture_);
	void OnInit(GInputYoshiAI* ai);
	void OnLoop(float delta_time);
	void OnRender(float extrap, SDL_Renderer* renderer);
	virtual void OnCleanup();

	void MoveHor(float delta_time);
	void OnMove(float delta_time);
	bool OnCollision(GEntity* Entity);
	

	bool GetGrounded();
	float GetSpeedX();
	float GetSpeedY();
	float GetFracX();
	float GetFracY();

	int GetRenderX(float extrapolate);
	int GetRenderY(float extrapolate);


	//protected:
		//movement
	int move = 0;

	float speed_x;
	float speed_y;

	float accel_x = 0;
	float accel_y = 0;

	float fraction_x = 0.0;
	float fraction_y = 0.0;

	float max_speed_x = 0.0;
	float max_speed_y = 0.0;

	//some values
	float grav = 0.75;
	float grav_max = 10;

	float walkspeed = 5;
	float walk_accel = 0.5;

	float runspeed = 10;
	float run_accel = 1;

	int jumpspeed = 12;

	//Time effects
	float timefactor = 1;

	//flags 
	bool grounded = false;
	bool jumping = false;

	float speed_x_next, speed_y_next;

	int x_prev, y_prev;

	int timer;

//private:

	//Input
	GBufferReader br;
	uint32_t btn;
	uint32_t btn_pressed;
	uint32_t btn_released;

	//Audio
	GSound* jumpsound;

	//Collision
	bool PosValid(int NewX, int NewY);
	bool PosValidCol(int new_x, int new_y);
	bool PosValidEntity(GEntity* Entity, int NewX, int NewY);

	bool TriangleCollision(SDL_Point a, SDL_Point b, SDL_Rect rect);
};

#endif