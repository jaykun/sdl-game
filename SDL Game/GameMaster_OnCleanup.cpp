#include "stdafx.h"
#include "GameMaster.h"
#include "GArea.h"

void GameMaster::OnCleanup()
{
	for (int i = 0; i < GEntity::entityList.size(); i++)
	{
		if (!GEntity::entityList[i]) continue;

		GEntity::entityList[i]->OnCleanup();
	}

	GEntity::entityList.clear();

	GArea::AreaControl.OnCleanup();

	for (int ControllerIndex = 0; ControllerIndex < MAX_CONTROLLERS; ++ControllerIndex)
	{
		if (gameControllers[ControllerIndex])
		{
			SDL_GameControllerClose(gameControllers[ControllerIndex]);
		}
	}

	for (int i = 0; i < 4; ++i)
	{
		inputs[i].OnCleanUp();
	}


	SDL_DestroyWindow(window);	
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}
