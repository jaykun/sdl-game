#pragma once
#ifndef GINPUTREPEAT_H_
#define GINPUTREPEAT_H_

#include <cstdint>
#include <iostream>

class GInputRepeat
{
public:
	GInputRepeat() = default;
	~GInputRepeat() = default;
	void SetTimelimit(uint32_t max_ms);
	void SetButton(uint32_t button);
	void Handle(uint32_t state_pressed);
	bool UpdateTime(uint64_t time_);
	void Active(bool val);
	bool Active();
private:
	uint64_t time = 0;
	uint64_t max_ms = 0;
	uint64_t max_time = 0;
	uint32_t button = 0;
	bool active = false;
};


#endif
