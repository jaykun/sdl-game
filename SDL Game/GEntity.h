#pragma once
#ifndef GENTITY_H_
#define GENTITY_H_

#include <vector>

#include "GArea.h"
#include "GAnimation.h"
#include "GSurface.h"
#include "GSprite.h"
#include "GTexture.h"
//#include "GCollider.h"

//Leftovers from SDL tutorials
enum {
	ENTITY_TYPE_GENERIC = 0,

	ENTITY_TYPE_PLAYER,
	ENTITY_TYPE_ENEMY
};

enum {
	ENTITY_FLAG_NONE = 0,

	ENTITY_FLAG_GRAVITY = 0x00000001,
	ENTITY_FLAG_GHOST = 0x00000002,
	ENTITY_FLAG_MAPONLY = 0x00000004
};

enum
{
	COLLISION_NONE,
	COLLISION_RECTANGLE,
	COLLISION_TRIANGLE,
	COLLISION_ELLIPSE,
	COLLISION_PERFECT
};

enum collider_type
{
	COL_NONE,
	COL_SOLID,
	COL_DOWNJUMP,
	COL_CHARACTER
};

enum collider_shape
{
	COL_SHAPE_RECTANGLE,
	COL_SHAPE_TRIANGLE,
	COL_SHAPE_ELLIPSE,
	COL_SHAPE_PIXEL_PERFECT
};

class GEntity {
public:
	GEntity();

	virtual ~GEntity();

	virtual bool OnLoad(GTexture &texture_);

	virtual bool SetCollisionMask(GTexture &texture_);

	virtual void OnInit();

	virtual void OnLoop(float delta_time);

	virtual void OnRender(float extrap, SDL_Renderer *renderer);

	virtual void OnCleanup();

	virtual bool OnCollision(GEntity* Entity);

	virtual bool Collides(int oX, int oY, int oW, int oH);
	virtual bool Collides2(int other_x1, int other_y1, int other_x2, int other_y2, int other_x3, int other_y3, uint8_t type_, uint8_t shape_);

	virtual int GetX();
	virtual int GetY();
	bool TriangleCollision(int tri_x1, int tri_y1, int tri_x2, int tri_y2, int rect_x, int rect_y, int rect_w, int rect_h);

	uint8_t shape = COL_SHAPE_RECTANGLE;
	uint8_t type = COL_NONE;

	int x;
	int y;

	//Collision
	int col_x[3];
	int col_y[3];

	////x/y for rectangle and ellipse
	//int col_x[0] = 0;
	//int col_y[0] = 0;
	////width/heightfor rectangle and ellipse
	//int col_x[1] = 0;
	//int col_y[1] = 0;
	////point3 x/y for triangle
	//int col_x[2] = 0;
	//int col_y[2] = 0;
	
	int width;
	int height;

	//Easier than checking via cast functions, might be useful after all
	uint8_t type2 = ENTITY_TYPE_GENERIC;

	//Flags might be a cool way to check multiple conditions at once.
	uint8_t flags = ENTITY_FLAG_GRAVITY;

	uint8_t col_type = COLLISION_NONE;

	//List of all entities, important
	static std::vector<GEntity*> entityList;

	//Whether to skip certain functionality
	bool loop = true;
	bool render = true;

	//public because state-tests
//protected:	
	//Texture
	GTexture* texture = nullptr;

	//Animation
	GAnimation Anim_Control;

	//Texture modifiers
	SDL_BlendMode texture_blend = SDL_BLENDMODE_BLEND;
	int texture_red = 255;
	int texture_green = 255;
	int texture_blue = 255;
	int texture_alpha = 255;

	float texture_angle = 0.0;
	SDL_RendererFlip texture_flip = SDL_FLIP_NONE;
};

class GEntityCol 
{
public:
	GEntityCol();

	GEntity* EntityA;
	GEntity* EntityB;

	static std::vector<GEntityCol> EntityColList;
};

class GWall : public GEntity
{
public:
	GWall();
	~GWall() = default;
	static std::vector<GEntity*> GWallList;
};

#endif