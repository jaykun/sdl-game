#pragma once
#ifndef CTILE_H_
#define CTILE_H_

#include "Define.h"

enum 
{
	TILE_TYPE_NONE = 0,
	TILE_TYPE_NORMAL,
	TILE_TYPE_BLOCK
};

class GTile 
{
public:
	GTile();

	int TileID;
	int TypeID;
};

#endif