#pragma once
#ifndef GINPUT_H_
#define GINPUT_H_

#include <SDL.h>

enum
{
	BUTTON_LEFT,
	BUTTON_RIGHT,
	BUTTON_UP,
	BUTTON_DOWN,
	BUTTON_SELECT,
	BUTTON_START,
	BUTTON_A,
	BUTTON_B,
	BUTTON_X,
	BUTTON_Y,
	BUTTON_LB,
	BUTTON_RB,
	BUTTON_LT,
	BUTTON_RT
};

class GInput
{
public:
	GInput() = default;
	virtual ~GInput();

	//Loop these
	virtual void OnInput(uint64_t current_time, float delta_time) = 0;
	virtual void FlushBufferBefore(uint64_t time);

	//Buffer access
	virtual uint32_t* GetStateBuffer();
	virtual uint64_t* GetTimestampBuffer();
	virtual uint8_t GetBufferStart();
	virtual uint8_t GetBufferIndex();

protected:
	uint32_t state[256];
	uint64_t timestamp[256];

	Uint8 buffer_start = 0;
	Uint8 buffer_index = 0;

	uint32_t prev_button_state = 0;
};



#endif