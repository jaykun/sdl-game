#pragma once
#ifndef GMENUINGAME_H_
#define GMENUINGAME_H_

#include "GInputPlayer.h"
#include "GCamera.h"

class GameMaster;

class GMenuIngame
{
public:
	void OnInit(GameMaster *game, TTF_Font *font);
	void OnLoop();
	void OnRenderBegin(SDL_Renderer *renderer);
	void OnRender(SDL_Renderer *renderer);

private:
	GameMaster *game;
	GBufferReader br;
	GInputPlayer *input[MAX_INPUTS];
	GTexture pause_texture;
	TTF_Font *menu_font;

	bool paused = false;
	bool paused_prev = false;

	//menu box borders
	int border_x = 0;
	int border_y = 0;
	int border_width = 200;
	int border_height = 200;

	//menu controls
	int current_item = 0;
	int max_items = 4;


};



#endif