#pragma once
#ifndef GAMEMASTER_H_
#define GAMEMASTER_H_

#include "SDL.h"

class GTime {
public:
	GTime();

	void Update();
	void UpdateBy(uint64_t _ui64Ticks);
	
	uint64_t CurTime() const;
	uint64_t CurMicros() const;
	uint64_t DeltaMicros() const;
	double DeltaSecs() const;

	void SetFrequency(uint64_t _ui64Resolution) 
	{
		m_ui64Resolution = _ui64Resolution;
	}

	void SynchronizeWith(const GTime& _tTime) 
	{
		m_ui64LastRealTime = _tTime.m_ui64LastRealTime;
	}
protected:
	uint64_t m_ui64Resolution = 0;
	uint64_t m_ui64CurTime = 0;
	uint64_t m_ui64LastTime = 0;
	uint64_t m_ui64LastRealTime = 0;
	uint64_t m_ui64CurMicros = 0;
	uint64_t m_ui64DeltaMicros = 0;
	double m_fDeltaSecs = 0;
};




#endif
