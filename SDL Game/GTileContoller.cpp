#include "stdafx.h"
#include "GTileController.h"
#include "GEllipse.h"
//#include "GLevelGeometry.h"
//#include <map>;



GTileController& GTileController::instance()
{
    static GTileController* instance = new GTileController();
    return *instance;
}

void GTileController::initialize(const fs::path& basePath)
{
    m_basePath = basePath;
}

std::vector<GYoshi*>& GTileController::GetMonsters()
{
    return m_monsters;
}
//std::vector<SDL_Rect>& GTileController::GetColRects()
//{
//    return m_col_rectangles;
//}
std::vector<GEllipse>& GTileController::GetColEllipses()
{
    return m_col_ellipses;
}
std::vector<SDL_Point>& GTileController::GetColTris()
{
    return m_col_triangles;
}

bool GTileController::parseMap(const std::string& filename, SDL_Renderer* renderer)
{
    tson::Tileson t;
    m_map = t.parse(fs::path(m_basePath / filename));

    if (m_map.getStatus() == tson::ParseStatus::OK)
    {
        for (auto& tileset : m_map.getTilesets())
            storeAndLoadImage(tileset.getImage().u8string(), { 0,0 }, renderer);

        return true;
    }
    else
        std::cout << "Parse error: " << m_map.getStatusMessage() << std::endl;

    return false;
}

/*!
 * Stores and loads the image if it doesn't exists, and retrieves it if it does.
 * @param image
 * @return
 */
GTexture* GTileController::storeAndLoadImage(const std::string& image, const std::tuple<int,int>& position, SDL_Renderer *renderer)
{
    if (m_textures.count(image) == 0)
    {
        fs::path path = m_basePath / image;
        
        if (fs::exists(path))
        {
            std::unique_ptr<GTexture> tex = std::make_unique<GTexture>();
            tex->OnLoad(image, renderer);
            m_textures[image] = std::move(tex);
        }
        else
            std::cout << "Could not find: " << path.u8string() << std::endl;
    }

    return m_textures[image].get(); //remove if trouble

    /*if (m_textures.count(image) > 0)
        return m_textures[image].get();

    return nullptr;*/
}

void GTileController::drawMap(SDL_Renderer* renderer)
{
    for (auto& layer : m_map.getLayers())
    {
        drawLayer(layer, renderer);
    }    
}


void GTileController::drawTileLayer(tson::Layer& layer, tson::Tileset* tileset, SDL_Renderer* renderer)
{
    int firstId = tileset->getFirstgid(); //First tile id of the tileset
    int columns = tileset->getColumns(); //For the demo map it is 8.
    int rows = tileset->getTileCount() / columns;
    int lastId = (tileset->getFirstgid() + tileset->getTileCount()) - 1;

    //pos = position in tile units
    for (auto& [pos, tile] : layer.getTileData()) //Loops through absolutely all existing tiles
    {        
        //With this, I know that it's related to the tileset above (though I only have one tileset)
        if (tile->getId() >= firstId && tile->getId() <= lastId)
        {
            //Get position in pixel units
            tson::Vector2f position = { (float)std::get<0>(pos) * m_map.getTileSize().x, (float)std::get<1>(pos) * m_map.getTileSize().y };

            int baseTilePosition = (tile->getId() - firstId);

            int tileModX = (baseTilePosition % columns);
            int currentRow = (baseTilePosition / columns);
            int offsetX = (tileModX != 0) ? ((tileModX)*m_map.getTileSize().x) : (0 * m_map.getTileSize().x);
            int offsetY = (currentRow < rows - 1) ? (currentRow * m_map.getTileSize().y) : ((rows - 1) * m_map.getTileSize().y);

            //Set sprite data to draw the tile
            GTexture* tex = storeAndLoadImage(tileset->getImage().u8string(), { 0,0 }, renderer);
            if (tex != nullptr)
            {
                tex->OnRender(position.x - camera_x, position.y - camera_y, offsetX, offsetY, m_map.getTileSize().x, m_map.getTileSize().y, renderer);
            }
        }
    }
}

void GTileController::drawImageLayer(tson::Layer& layer, SDL_Renderer* renderer)
{
    GTexture* tex = storeAndLoadImage(layer.getImage(), { layer.getOffset().x, layer.getOffset().y }, renderer);
    if (tex != nullptr)       
        tex->OnRender(layer.getOffset().x - camera_x, layer.getOffset().y - camera_y, 0, 0, tex->GetWidth(), tex->GetHeight(), renderer);
}

void GTileController::drawObjectLayer(tson::Layer& layer, SDL_Renderer* renderer)
{
    tson::Tileset* tileset = m_map.getTileset("demo-tileset");

    for (auto& obj : layer.getObjects())
    {
        switch (obj.getObjectType())
        {
        case tson::ObjectType::Object:
        {
            tson::Vector2i offset = getTileOffset(obj.getGid());
            GTexture* tex = storeAndLoadImage(tileset->getImage().u8string(), { 0,0 }, renderer);
            std::string name = obj.getName();
            if (tex != nullptr)
            {
                tex->OnRender(obj.getPosition().x - camera_x, obj.getPosition().y - camera_y, offset.x, offset.y, m_map.getTileSize().x, m_map.getTileSize().y, renderer);
            }
        }
        break;

        case tson::ObjectType::Ellipse:
            //Not used by the demo map, but you could use the properties of obj for a sf::CircleShape
            break;

        case tson::ObjectType::Rectangle:
        {
            tson::Vector2i pos = obj.getPosition();
            tson::Vector2i size = obj.getSize();

            /*std::string name = obj.getName();
            if (name == "lol")
            {
                std::cout << pos.y - camera_y << std::endl;
                std::cout << camera_y << std::endl;
            }*/
            

            SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // the rect color (solid red)

            SDL_Rect rect;

            rect.x = pos.x -camera_x;
            rect.y = pos.y -camera_y;
            rect.w = size.x;
            rect.h = size.y;

            SDL_RenderDrawRect(renderer, &rect);

        }
            break;

        case tson::ObjectType::Point:
            //Not used by the demo map but one could use the points of obj (polygon or polyline)
            //then pass them into logic like this:
            //sf::Vertex line[] =
            //        {
            //                sf::Vertex(sf::Vector2f(obj.getPolylines()[0].x, obj.getPolylines()[0].y)),
            //                sf::Vertex(sf::Vector2f(obj.getPolylines()[1].x, obj.getPolylines()[1].y))
            //        };
            //m_window.draw(line, 2, sf::Lines);
            break;

        case tson::ObjectType::Polygon:
            //Not used by the demo map, but you could use the properties of obj for a sf::ConvexShape
            break;

        case tson::ObjectType::Polyline:
            //Not used by the demo map, but you could use the properties of obj for a sf::ConvexShape
            break;

        case tson::ObjectType::Text:
            //m_demoText.setFont(m_font);
            //m_demoText.setPosition({ (float)obj.getPosition().x, (float)obj.getPosition().y });
            //m_demoText.setString(obj.getText().text);
            //m_demoText.setCharacterSize(32); //It is 16, but makes it double for a "sharp text hack"
            //m_demoText.setScale(0.5f, 0.5f); //Half scale for making a sharp text.
            //m_window.draw(m_demoText);
            break;

        case tson::ObjectType::Template:
            //use obj.getTemplate() to get the connected template. References an external file not covered by Tileson.
            //obj.getPosition() and obj.getId() should also be related to the placement of the template.

            break;

        default:
            break;
        }
    }
}

void GTileController::drawLayer(tson::Layer& layer, SDL_Renderer* renderer)
{
    tson::Tileset* tileset = m_map.getTileset("demo-tileset");
    switch (layer.getType())
    {
    case tson::LayerType::TileLayer:
        drawTileLayer(layer, tileset, renderer);
        break;

    case tson::LayerType::ObjectGroup:
        drawObjectLayer(layer, renderer);
        break;

    case tson::LayerType::ImageLayer:
        drawImageLayer(layer, renderer);
        break;

    case tson::LayerType::Group:
        //There are no group layers in the demo map, but it basicly just contains sub layers
        //You can call this function on those layers, like this:
        for (auto& l : layer.getLayers())
            drawLayer(l, renderer);

        break;

    default:
        break;
    }
}

tson::Vector2i GTileController::getTileOffset(int tileId)
{
    tson::Tileset* tileset = m_map.getTileset("demo-tileset");
    int firstId = tileset->getFirstgid(); //First tile id of the tileset
    int columns = tileset->getColumns(); //For the demo map it is 8.
    int rows = tileset->getTileCount() / columns;
    int lastId = (tileset->getFirstgid() + tileset->getTileCount()) - 1;

    //With this, I know that it's related to the tileset above (though I only have one tileset)
    if (tileId >= firstId && tileId <= lastId)
    {
        int baseTilePosition = (tileId - firstId);

        int tileModX = (baseTilePosition % columns);
        int currentRow = (baseTilePosition / columns);
        int offsetX = (tileModX != 0) ? ((tileModX)*m_map.getTileSize().x) : (0 * m_map.getTileSize().x);
        int offsetY = (currentRow < rows - 1) ? (currentRow * m_map.getTileSize().y) : ((rows - 1) * m_map.getTileSize().y);
        return tson::Vector2i(offsetX, offsetY);
    }

    return { 0, 0 };
}

void GTileController::scanObjectLayer(GMemoryManager& mem_mgr)
{
    //We could have another similiar loop here, just to map out what type of entities the map contains
    //Then create a pool allocator for each of these types

   /* std::map<std::string, uint16_t> objects;
    for (auto& layer : m_map.getLayers())
    {
        if (layer.getType() == tson::LayerType::ObjectGroup)
        {
            for (auto& obj : layer.getObjects())
            {
                std::string type = obj.getType();
                auto it = objects.find(type);
                if (it != objects.end())
                {
                    objects.emplace(type, 1);
                }
                else
                {
                    it->second++;
                }
            }
        }
    }*/

    for (auto& layer : m_map.getLayers())
    {
        if (layer.getType() == tson::LayerType::ObjectGroup)
        {
            for (auto& obj : layer.getObjects())
            {
                switch (obj.getObjectType())
                {
                case tson::ObjectType::Object:   
                    if (obj.getType() == "Yoshi")
                    {

                        if (mem_mgr.pool_gyoshi == nullptr)
                        {
                            void* mem_block = mem_mgr.main_allocator->allocate(sizeof(GYoshi) * mem_mgr.pool_gyoshi_size, 2);
                            void* pool_block = mem_mgr.meta_allocator->allocate(sizeof(PoolAllocator), sizeof(PoolAllocator));

                            mem_mgr.pool_gyoshi = new(pool_block) PoolAllocator(sizeof(GYoshi), sizeof(GYoshi), sizeof(GYoshi) * mem_mgr.pool_gyoshi_size, mem_block);
                        }

                        tson::Vector2i pos = obj.getPosition();

                        void* pieni = mem_mgr.pool_gyoshi->allocate(sizeof(GYoshi), sizeof(GYoshi));
                        m_monsters.push_back(new(pieni) GYoshi());
                        m_monsters[0]->x = pos.x;
                        m_monsters[0]->y = pos.y;
                    }
                    break;

                case tson::ObjectType::Ellipse:
                    if (obj.getType() == "wall")
                    {
                        tson::Vector2i pos = obj.getPosition();
                        tson::Vector2i size = obj.getSize();      

                        GEllipse ellipse;
                        ellipse.x = pos.x;
                        ellipse.y = pos.y;
                        ellipse.w = size.x;

                        m_col_ellipses.push_back(ellipse);
                    }
                    break;

                case tson::ObjectType::Rectangle:
                    if (obj.getType() == "GWall")
                    {                       
                        tson::Vector2i pos = obj.getPosition();
                        tson::Vector2i size = obj.getSize();

                        if (mem_mgr.entity_allocator == nullptr)
                        {
                            void* mem_block = mem_mgr.main_allocator->allocate(mem_mgr.entity_allocator_size, 2);
                            void* pool_block = mem_mgr.meta_allocator->allocate(sizeof(StackAllocator), sizeof(StackAllocator));

                            mem_mgr.entity_allocator = new(pool_block) StackAllocator(mem_mgr.entity_allocator_size, mem_block);
                        }

                        void* mem = mem_mgr.entity_allocator->allocate(sizeof(GEntity), sizeof(GEntity));

                        GWall* wall = new(mem) GWall();

                        //GEntity* wall = new GEntity();


                        wall->x = pos.x;
                        wall->y = pos.y;
                        wall->width = size.x;
                        wall->height = size.y;
                        wall->type = COL_SOLID;
                        wall->shape = COL_SHAPE_RECTANGLE;

                        /*SDL_Rect rect;
                        rect.x = pos.x;
                        rect.y = pos.y;
                        rect.w = size.x;
                        rect.h = size.y;

                        m_col_rectangles.push_back(rect);*/
                    }

                    break;

                case tson::ObjectType::Point:

                    break;

                case tson::ObjectType::Polygon:
                    if (obj.getType() == "GWall")
                    {
                        tson::Vector2i pos = obj.getPosition();
                        std::vector<tson::Vector2i> polys = obj.getPolygons();

                        if (polys.size() != 3)
                            break;

                        if (mem_mgr.entity_allocator == nullptr)
                        {
                            void* mem_block = mem_mgr.main_allocator->allocate(mem_mgr.entity_allocator_size, 2);
                            void* pool_block = mem_mgr.meta_allocator->allocate(sizeof(StackAllocator), sizeof(StackAllocator));

                            mem_mgr.entity_allocator = new(pool_block) StackAllocator(mem_mgr.entity_allocator_size, mem_block);
                        }

                        void* mem = mem_mgr.entity_allocator->allocate(sizeof(GEntity), sizeof(GEntity));
                        GWall* wall = new(mem) GWall();

                        wall->x = pos.x;
                        wall->y = pos.y;
                        wall->col_x[0] = /*pos.x +*/ polys[0].x;
                        wall->col_y[0] = /*pos.y +*/ polys[0].y;
                        wall->col_x[1] = /*pos.x +*/ polys[1].x;
                        wall->col_y[1] = /*pos.y +*/ polys[1].y;
                        wall->col_x[2] = /*pos.x +*/ polys[2].x;
                        wall->col_y[2] = /*pos.y +*/ polys[2].y;
                        wall->type = COL_SOLID;
                        wall->shape = COL_SHAPE_TRIANGLE;

                        /*SDL_Point point;
                        for (int i = 0; i < polys.size(); i++)
                        {
                            point.x = pos.x + polys[i].x;
                            point.y = pos.y + polys[i].y;
                            m_col_triangles.push_back(point);
                        }*/

                    }
                    break;

                case tson::ObjectType::Polyline:
                    //Not used by the demo map, but you could use the properties of obj for a sf::ConvexShape
                    break;

                case tson::ObjectType::Text:
                    //m_demoText.setFont(m_font);
                    //m_demoText.setPosition({ (float)obj.getPosition().x, (float)obj.getPosition().y });
                    //m_demoText.setString(obj.getText().text);
                    //m_demoText.setCharacterSize(32); //It is 16, but makes it double for a "sharp text hack"
                    //m_demoText.setScale(0.5f, 0.5f); //Half scale for making a sharp text.
                    //m_window.draw(m_demoText);
                    break;

                case tson::ObjectType::Template:
                    //use obj.getTemplate() to get the connected template. References an external file not covered by Tileson.
                    //obj.getPosition() and obj.getId() should also be related to the placement of the template.

                    break;

                default:
                    break;
                }
            }
        }

    }    
}