#pragma once
#ifndef GAMEMASTER_H_
#define GAMEMASTER_H_

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
//#include <SDL_ttf.h>
#include <iostream>
#include <string>
#include "GEvent.h"
#include "GAnimation.h"
#include "GEntity.h"
#include "GPLayer.h"
#include "GTexture.h"
#include "GInputPlayer.h"
#include "GPLayer.h"
#include "GMenuIngame.h"
#include "GBufferReader.h"
#include "GTileController.h"
#include "GEllipse.h"
#include "FreeListAllocator.h"
#include "LinearAllocator.h"
#include "GMemoryManager.h"

class GameMaster : public GEvent
{
public:
	GameMaster();
	int OnExecute();
	std::string getBaseDir(const std::string &subDir = "");

	bool OnInit();
	void OnEvent(SDL_Event *event);

		void OnControllerDeviceAdded(Uint32 type, int which);
		void OnControllerDeviceRemoved(Uint32 type, int which);
		void OnExit();

	void CalculateDelta();
	void OnInput();
	void OnLoop();
	void OnRender();
	void OnCleanup();

	void SetLoop(bool value);
	void SetRender(bool value);
	double render_extrapolate = 0;
private:
	//SDL Basics
	bool running;
	SDL_Window *window = nullptr;
	SDL_Surface* windowSurface = nullptr;
	SDL_Renderer* renderer;

	SDL_Texture* render_target;

	//Memory allocation
	size_t mem_size = 100 * 1024 * 1024;
	FreeListAllocator mem_allocator;	
	StackAllocator meta_allocator;
	GMemoryManager mem_mgr;

	LinearAllocator test_allocator;

	//OnCalculateDelta()
	uint64_t ui64_resolution;
	uint64_t current_time;
	uint64_t last_time;
	uint64_t u_time_accumulator = 0;
	uint64_t frame_time_microseconds;

	float frames_per_second;
	float delta_time = 1.0f;
	float time_factor = 1.0f;	
	float step_interval = 1.0f;
	float time_accumulator = 0.0f;

	//for FPS counter
	uint64_t old_time = 0;
	int fps;
	int frames;
	float loop_fps;
	float loop_frames;	

	//loop/render control
	bool loop = true;
	bool render = true;

	//for interpolation between current and previous frame
	float alpha = 0;

	//controllers and input
	SDL_GameController* gameControllers[MAX_CONTROLLERS];
	GInputPlayer inputs[4];
	GInputYoshiAI* enemyAI;
	GMenuIngame ingame_menu;
	
	//yoshi texture
	GTexture text;

	//font and texture for font usage
	TTF_Font* testfont;
	GTexture text_font;

	GSound jumpsound;

	GPlayer Player;
};























#endif // !
