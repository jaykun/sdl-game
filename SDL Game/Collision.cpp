#include "stdafx.h"
#include "Collision.h"

bool TriangleCollision(int tri_x1, int tri_y1, int tri_x2, int tri_y2, int rect_left, int rect_top, int rect_right, int rect_bottom)
{
	int m = (tri_y2 - tri_y1) / (tri_x2 - tri_x1);
	int c = tri_y1 - (m * tri_x1);

	int top_intersection = 0;
	int bottom_intersection = 0;

	int toptrianglepoint;
	int bottomtrianglepoint;

	if (m > 0)
	{
		top_intersection = (m * rect_left + c);
		bottom_intersection = (m * (rect_right)+c);
	}
	else
	{
		top_intersection = (m * (rect_right)+c);
		bottom_intersection = (m * rect_left + c);
	}

	if (tri_y1 < tri_y2)
	{
		toptrianglepoint = tri_y1;
		bottomtrianglepoint = tri_y2;
	}
	else
	{
		toptrianglepoint = tri_y2;
		bottomtrianglepoint = tri_y1;
	}

	int topoverlap = top_intersection > toptrianglepoint ? top_intersection : toptrianglepoint;
	int botoverlap = bottom_intersection < bottomtrianglepoint ? bottom_intersection : bottomtrianglepoint;

	return (topoverlap < botoverlap) && (!((botoverlap < rect_top) || (topoverlap > (rect_bottom))));
}

float Sign(int p1_x, int p1_y, int p2_x, int p2_y, int p3_x, int p3_y)
{
	return (p1_x - p3_x) * (p2_y - p3_y) - (p2_x - p3_x) * (p1_y - p3_y);
}

bool IsPointInTri(int pt_x, int pt_y, int v1_x, int v1_y, int v2_x, int v2_y, int v3_x, int v3_y)
{
	bool b1 = (pt_x - v2_x) * (v1_y - v2_y) - (v1_x - v2_x) * (pt_y - v2_y);
	bool b2 = (pt_x - v3_x) * (v2_y - v3_y) - (v2_x - v3_x) * (pt_y - v3_y);
	bool b3 = (pt_x - v1_x) * (v3_y - v1_y) - (v3_x - v1_x) * (pt_y - v1_y);

	/*bool b1 = Sign(pt_x, pt_y, v1_x, v1_y, v2_x, v2_y) < 0;
	bool b2= Sign(pt_x, pt_y, v2_x, v2_y, v3_x, v3_y) < 0;
	bool b3 = Sign(pt_x, pt_y, v3_x, v3_y, v1_x, v1_y) < 0;*/
	return ((b1 == b2) && (b2 == b3));
}


//point in collisionbox?
bool Collision(int x, int y, GEntity* e)
{
	//calculate bounding boxes for objects. 1 = own, 2 = other

	int left = e->x + e->col_x[0];
	int right = left + e->width - 1 - e->col_x[1];
	int top = e->y + e->col_y[0];
	int bottom = top + e->height - 1 - e->col_y[1];

	switch (e->shape)
	{
	case COL_SHAPE_RECTANGLE:
		if (y < top) return false;
		if (y > bottom) return false;

		if (x < left) return false;
		if (x > right) return false;

		return true;
	case COL_SHAPE_ELLIPSE:

	case COL_SHAPE_TRIANGLE:
		int e1_x = e->x + e->col_x[0];
		int e1_y = e->y + e->col_y[0];
		int e2_x = e->x + e->col_x[1];
		int e2_y = e->y + e->col_y[1];
		int e3_x = e->x + e->col_x[2];
		int e3_y = e->y + e->col_y[2];

		IsPointInTri(x, y, e1_x, e1_y, e2_x, e2_y, e3_x, e3_y);
	}
}


//Collision between collision boxes?
bool Collision(GEntity* a, GEntity* b, int x, int y)
{
	//calculate bounding boxes for objects. 1 = own, 2 = other
	int a_left, b_left;
	int a_right, b_right;
	int a_top, b_top;
	int a_bottom, b_bottom;

	int a1_x;
	int a1_y;
	int a2_x;
	int a2_y;
	int a3_x;
	int a3_y;

	int b1_x;
	int b1_y;
	int b2_x;
	int b2_y;
	int b3_x;
	int b3_y;

	switch (a->shape)
	{
	case COL_SHAPE_RECTANGLE:
		switch (b->shape)
		{
		case COL_SHAPE_RECTANGLE:

			a_left = x + a->col_x[0];
			b_left = b->x + b->col_x[0];

			a_right = a_left + a->width - 1 - a->col_x[1];
			b_right = b_left + b->width - 1 - b->col_x[1];

			a_top = y + a->col_y[0];
			b_top = b->y + b->col_y[0];

			a_bottom = a_top + a->height - 1 - a->col_y[1];
			b_bottom = b_top + b->height - 1 - b->col_y[1];

			//If objects cannot touch each other, return false
			if (a_bottom < b_top) return false;
			if (a_top > b_bottom) return false;

			if (a_right < b_left) return false;
			if (a_left > b_right) return false;

			//if above statements did not return false, the objects are colliding
			return true;
		case COL_SHAPE_ELLIPSE:

			return false;
		case COL_SHAPE_TRIANGLE:
			a_left = x + a->col_x[0];
			a_right = a_left + a->width - 1 - a->col_x[1];
			a_top = y + a->col_y[0];
			a_bottom = a_top + a->height - 1 - a->col_y[1];

			b1_x = b->x + b->col_x[0];
			b1_y = b->y + b->col_y[0];
			b2_x = b->x + b->col_x[1];
			b2_y = b->y + b->col_y[1];
			b3_x = b->x + b->col_x[2];
			b3_y = b->y + b->col_y[2];

			if (TriangleCollision(b1_x, b1_y, b2_x, b2_y, a_left, a_top, a_right, a_bottom) ||
				TriangleCollision(b2_x, b2_y, b3_x, b3_y, a_left, a_top, a_right, a_bottom) ||
				TriangleCollision(b3_x, b3_y, b1_x, b1_y, a_left, a_top, a_right, a_bottom))
				return true;

			return false;
		}
	case COL_SHAPE_TRIANGLE:
		switch (b->shape)
		{
		case COL_SHAPE_RECTANGLE:

			b_left = b->x + b->col_x[0];
			b_right = b_left + b->width - 1 - b->col_x[1];
			b_top = b->y + b->col_y[0];
			b_bottom = b_top + b->height - 1 - b->col_y[1];

			a1_x = a->x + a->col_x[0];
			a1_y = a->y + a->col_y[0];
			a2_x = a->x + a->col_x[1];
			a2_y = a->y + a->col_y[1];
			a3_x = a->x + a->col_x[2];
			a3_y = a->y + a->col_y[2];

			if (TriangleCollision(a1_x, a1_y, a2_x, a2_y, b_left, b_top, b_right, b_bottom) ||
				TriangleCollision(a2_x, a2_y, a3_x, a3_y, b_left, b_top, b_right, b_bottom) ||
				TriangleCollision(a3_x, a3_y, a1_x, a1_y, b_left, b_top, b_right, b_bottom))
				return true;

			return false;
		case COL_SHAPE_ELLIPSE:
			//triangle to ellipse algorithm needed
			return false;
		case COL_SHAPE_TRIANGLE:
			//triangle to triangle algorithm needed
			return false;
		}
	}
}



bool CollisionAtPlace(GEntity* e, int x, int y, std::vector<GEntity*>& list)
{
	for (int i = 0; i < list.size(); i++)
	{
		if (Collision(e, list[i], x, y))
			return true;
	}

	return false;
}

GEntity* InstanceAtPlace(int x, int y, std::vector<GEntity*>& list)
{
	for (int i = 0; i < list.size(); i++)
	{
		if (Collision(x, y, list[i]))
			return list[i];
	}

	return nullptr;
}