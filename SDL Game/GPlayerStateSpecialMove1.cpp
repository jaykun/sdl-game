#include "stdafx.h"
#include "GPlayerStateSpecialMove1.h"
#include "GPlayerStateNormalLoop.h"
#include "HelperFunctions.h"

#define Bit(a) 1U << a
#define GetBtn(a,b) (bool)(a & 1U << b)


GPlayerStateSpecialMove1::GPlayerStateSpecialMove1(GPlayer* player)
{
	m_player = player;
}

GState* GPlayerStateSpecialMove1::OnInit(float delta_time)
{
	//NOTE: all speeds should be multiplied by delta_time
	//no they shouldn't
	m_player->max_speed_x = 30 /* delta_time*/;
	
	//no vertical movement required
	m_player->max_speed_y = 0;
	m_player->speed_y = 0;
	m_player->accel_y = 0;

	//otherwise fraction additions can cause the player to go over destination x
	m_player->fraction_x = 0;
	m_player->fraction_y = 0;

	dir = m_player->Anim_Control.CurrentFrameCol == 0 ? -1 : 1;
	dest_x = m_player->x + dir * 700;
	m_player->speed_x = dir * 30;

	return nullptr;
}

GState* GPlayerStateSpecialMove1::OnLoop(float delta_time)
{
	if (m_player->speed_x == 0)
		return new GPlayerStateNormalLoop(m_player);

	if (m_player->x != dest_x)
	{
		int temp_speed = abs(dest_x - m_player->x) /* delta_time*/;		

		if (temp_speed < abs(m_player->speed_x /* delta_time*/))
			m_player->speed_x = dir * temp_speed;

		m_player->OnMove(delta_time);
	}
	else
		return new GPlayerStateNormalLoop(m_player);

	return nullptr;
}

void GPlayerStateSpecialMove1::OnEnd()
{
	m_player->max_speed_x = 0;
	m_player->speed_x = 0;
}