#pragma once
#include "Allocator.h" 
#include "PointerMath.h"

class LinearAllocator : public Allocator
{
public:
	LinearAllocator() = default;
	LinearAllocator(size_t size, void* start);
	~LinearAllocator();

	void init(size_t size, void* start);
	void* allocate(size_t size, u8 alignment) override;
	void deallocate(void* p) override;
	void clear();

private:

	LinearAllocator(const LinearAllocator&);

	//Prevent copies because it might cause errors 
	LinearAllocator& operator=(const LinearAllocator&);
	void* _current_pos;
};