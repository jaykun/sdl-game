#pragma once
#ifndef GPLAYERSTATEJUMPAIRWALK_H_
#define GPLAYERSTATEJUMPAIRWALK_H_


#include "GState.h"

class GPlayerStateJumpAirWalk : public GState
{
public:
	GPlayerStateJumpAirWalk(GPlayer* player);// = default;
	virtual ~GPlayerStateJumpAirWalk() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif