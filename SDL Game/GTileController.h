#pragma once
#ifndef GTILECONTROLLER_H_
#define GTILECONTROLLER_H_

#include "GTexture.h"
#include "tileson.hpp"
#include "GEllipse.h"
//test
#include "GYoshi.h"
#include "LinearAllocator.h"
#include "GMemoryManager.h"

class GTileController
{
public:
	static GTileController& instance();
	void initialize(const fs::path& basePath = fs::path("../../../../../content/test-maps/"));
	bool parseMap(const std::string& filename = "sdlgamemap.json", SDL_Renderer* renderer = nullptr);
	void drawMap(SDL_Renderer* renderer);

	void scanObjectLayer(GMemoryManager& mem_mgr);
	//std::vector<SDL_Rect>& GetColRects();
	std::vector<GEllipse>& GetColEllipses();
	std::vector<SDL_Point>& GetColTris();
	std::vector<GYoshi*>& GetMonsters();

	//singleton pattern doesn't support these
	GTileController(GTileController const&) = delete;
	void operator=(GTileController const&) = delete;
	
	int camera_x, camera_y;
private:
	GTileController() {}

	void drawLayer(tson::Layer& layer, SDL_Renderer* renderer);
	void drawTileLayer(tson::Layer& layer, tson::Tileset* tileset, SDL_Renderer* renderer);
	void drawImageLayer(tson::Layer& layer, SDL_Renderer* renderer);
	void drawObjectLayer(tson::Layer& layer, SDL_Renderer* renderer);

	
	tson::Vector2i getTileOffset(int tileId);

	GTexture* storeAndLoadImage(const std::string& image, const std::tuple<int, int>& position, SDL_Renderer* renderer);

	tson::Map m_map;
	fs::path m_basePath;

	std::map<std::string, std::unique_ptr<GTexture>> m_textures;

	std::vector<SDL_Rect> m_col_rectangles;
	std::vector<GEllipse> m_col_ellipses;
	std::vector<SDL_Point> m_col_triangles;
	//test
	std::vector<GYoshi*> m_monsters;
};






#endif