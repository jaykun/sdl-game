#pragma once
#ifndef COLLISION_H_
#define COLLISION_H_

#include "GEntity.h"

bool TriangleCollision(int tri_x1, int tri_y1, int tri_x2, int tri_y2, int rect_left, int rect_top, int rect_right, int rect_bottom);

float Sign(int p1_x, int p1_y, int p2_x, int p2_y, int p3_x, int p3_y);

bool IsPointInTri(int pt_x, int pt_y, int v1_x, int v1_y, int v2_x, int v2_y, int v3_x, int v3_y);

//point in collisionbox?
bool Collision(int x, int y, GEntity* e);

//Collision between collision boxes?
bool Collision(GEntity* a, GEntity* b, int x, int y);

bool CollisionAtPlace(GEntity* e, int x, int y, std::vector<GEntity*>& list);

GEntity* InstanceAtPlace(int x, int y, std::vector<GEntity*>& list);


#endif

