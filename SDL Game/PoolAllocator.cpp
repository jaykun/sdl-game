#include "stdafx.h"
#include "PoolAllocator.h" 
#include <iostream>

PoolAllocator::PoolAllocator(size_t objectSize, u8 objectAlignment, size_t size, void* mem) : Allocator(size, mem), _objectSize(objectSize), _objectAlignment(objectAlignment)
{
    assert(objectSize >= sizeof(void*));

    //Calculate adjustment needed to keep object correctly aligned 
    u8 adjustment = pointer_math::alignForwardAdjustment(mem, objectAlignment);

    //cast returned pointer to void-typed memory as a pointer to a pointer to void-typed memory
    //thus, the value found at the value location is interpreted as a pointer, even though it is uninitialized
    _free_list = (void**)pointer_math::add(mem, adjustment);

    size_t numObjects = (size - adjustment) / objectSize;

    //p starts at 
    void** p = _free_list;

    //Initialize free blocks list 
    //...so the start of each object memory space has the address of the next one
    for (size_t i = 0; i < numObjects - 1; i++)
    {
        //the address of the next object is found in the value space of void*
        *p = pointer_math::add(p, objectSize);
        //address space is set to value space pointer that was updated above
        p = (void**)*p;
    }

    //why do this? scope ends here anyway
    *p = nullptr;
}

PoolAllocator::~PoolAllocator()
{
    _free_list = nullptr;
}

void* PoolAllocator::allocate(size_t size, u8 alignment)
{
    assert(size == _objectSize && alignment == _objectAlignment);
    if (_free_list == nullptr) return nullptr;
    void* p = _free_list;
    _free_list = (void**)(*_free_list);
    _used_memory += size;
    _num_allocations++;
    return p;
}

void PoolAllocator::deallocate(void* p)
{
    *((void**)p) = _free_list;
    _free_list = (void**)p;
    _used_memory -= _objectSize;
    _num_allocations--;
}