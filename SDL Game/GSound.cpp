#include "stdafx.h"
#include "GSound.h"

std::unordered_map<std::string, GSound*> GSound::sound_list{};

GSound *GSound::GetSound(const std::string name)
{
	auto sound = sound_list.find(name);

	if (sound == sound_list.end())
	{
		return nullptr;
	}
	else
	{
		return sound->second;
	}
}

GSound::GSound(std::string path)
{
	std::string imgPath = SDL_GetBasePath();
	imgPath += path;

	sound = Mix_LoadWAV(path.c_str());

	if (sound == nullptr)
	{
		printf("Failed to load sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		return;
	}

	sound_list.insert({ path, this });
}

void GSound::OnLoad(std::string path)
{
	Free();

	std::string sndPath = SDL_GetBasePath();
	sndPath += path;

	sound = Mix_LoadWAV(sndPath.c_str());

	if (sound == nullptr)
	{
		printf("Failed to load sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		return;
	}

	sound_list.insert({ path, this });
}

void GSound::SetChannel(int ch)
{
	channel = ch;
}

int GSound::GetChannel()
{
	return channel;
}

void GSound::Play(int loops)
{
	Mix_PlayChannel(channel, sound, loops);
}

void GSound::Free()
{
	if (sound != nullptr)
	{
		Mix_FreeChunk(sound);
		sound = nullptr;
	}

	return;
}

GSound::~GSound()
{
	Free();
}