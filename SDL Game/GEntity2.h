#pragma once
#ifndef GENTITY2_H_
#define GENTITY2_H_

#include <vector>

#include "GArea.h"
#include "GAnimation.h"
//#include "GCamera.h"
#include "GFPS.h"
#include "GSurface.h"
#include "GSprite.h"
#include "GTexture.h"

//prototype for possible component oriented game entity
class GEntity2 {
public:
	GEntity2();

	virtual ~GEntity2();

	virtual bool OnLoad(GTexture &texture_);

	virtual bool SetCollisionMask(GTexture &texture_);

	virtual void OnInit();

	virtual void OnLoop();

	virtual void OnRender(SDL_Renderer *renderer);

	virtual void OnCleanup();

	virtual void OnAnimate();

	virtual bool OnCollision(GEntity2* Entity);

	virtual bool Collides(int oX, int oY, int oW, int oH);

	virtual bool ReceivesInput();

	virtual int GetX();

	virtual int GetY();

	double x;
	double y;

	int width;
	int height;

	//Movement
	double speed_x;
	double speed_y;

	//Easier than checking via cast functions, might be useful after all
	int Type;

	//Flags might be a cool way to check multiple conditions at once.
	int Flags;

protected:
	//GInput* input;
	//better like this than with an interface

	//GScript* script;
	//would include OnInit(), OnLoop(), OnMove(), OnCollision(), Collides() and functionality of GAnimation

	//GRenderer* render;
	//would just include OnRender

private:
};

#endif