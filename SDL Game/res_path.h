#pragma once
#ifndef RES_PATH_H
#define RES_PATH_H

#include <iostream>
#include <string>
#include <SDL.h>

/*
* Get the resource path for resources located in res/subDir
* It's assumed the project directory is structured like:
* bin/
*  the executable
* res/
*  Lesson1/
*  Lesson2/
*
* Paths returned will be Project_Root/res/subDir
*/
std::string getResourcePath(const std::string &subDir = "") 
{
#ifdef _WIN32
	const char PATH_SEP = '\\';
#else
	const char PATH_SEP = '/';
#endif
	static std::string baseRes;
	if (baseRes.empty()) {
		char *basePath = SDL_GetBasePath();
		if (basePath) {
			baseRes = basePath;
			SDL_free(basePath);
		}
		else {
			std::cerr << "Error getting resource path: " << SDL_GetError() << std::endl;
			return "";
		}
		//We replace the last bin/ with res/ to get the the resource path
		size_t pos = baseRes.rfind("bin");
		baseRes = baseRes.substr(0, pos) + "res" + PATH_SEP;
	}
	return subDir.empty() ? baseRes : baseRes + subDir + PATH_SEP;
}

std::string getBaseDir(const std::string &subDir = "")
{
#ifdef _WIN32
	const char PATH_SEP = '\\';
#else
	const char PATH_SEP = '/';
#endif
	std::string baseD(SDL_GetBasePath());
	return subDir.empty() ? baseD : baseD + subDir /*+ PATH_SEP*/;
}


//Doesn't work, needs to return 'new char' pointer
const char* getFile(const std::string &file = "")
{
#ifdef _WIN32
	const char PATH_SEP = '\\';
#else
	const char PATH_SEP = '/';
#endif
	std::string baseD(SDL_GetBasePath());

	return file.empty() ? baseD.c_str() : (baseD + file).c_str();
}

int signOf(int x)
{
	return (x > 0) - (x < 0);
}





#endif
