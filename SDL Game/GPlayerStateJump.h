#pragma once
#ifndef GPLAYERSTATEJUMP_H_
#define GPLAYERSTATEJUMP_H_


#include "GState.h"

class GPlayerStateJump : public GState
{
public:
	GPlayerStateJump(GPlayer* player);// = default;
	virtual ~GPlayerStateJump() = default;
	virtual GState* OnInit(float delta_time);
	virtual GState* OnLoop(float delta_time);
	virtual void OnEnd();


protected:


private:


};

#endif