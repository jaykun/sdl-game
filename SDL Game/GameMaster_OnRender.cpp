#include "stdafx.h"

#include "GameMaster.h"
#include "GSurface.h"
#include "GCamera.h"
#include "GArea.h"

void GameMaster::OnRender()
{
	//clear screen with black	
	SDL_SetRenderDrawColor(renderer, 0x0, 0x0, 0x0, 0xFF);
	SDL_RenderClear(renderer);

	//a separate OnRenderBegin() and OnRenderEnd() loop might be useful if visual effects become more complex
	//right now those are only necessary for pause function

	//here we set the render target to pause texture if game is paused
	ingame_menu.OnRenderBegin(renderer);

	if (render)
	{
		//not used anywhere, probably for testing something previously
		int cam_prev_x = GCamera::CameraControl.GetX();
		int cam_prev_y = GCamera::CameraControl.GetY();

		//Camera needs to be updated exactly so things render at proper location with extrapolation
		GCamera::CameraControl.OnLoop(delta_time, time_accumulator, time_factor);

		//Render area/maps/tiles. Move them according to camera location
		GTileController::instance().camera_x = GCamera::CameraControl.GetX();
		GTileController::instance().camera_y = GCamera::CameraControl.GetY();
		GTileController::instance().drawMap(renderer);
		//Render entities in entitylist
		for (int i = 0; i < GEntity::entityList.size(); i++)
		{
			if (!GEntity::entityList[i]) continue;

			GEntity::entityList[i]->OnRender(time_accumulator, renderer);
		}

		//For displaying camera debug information
		GCamera::CameraControl.OnRender(renderer);

		//GCamera::CameraControl.SetPos(cam_prev_x, cam_prev_y);
	}
	
	//if game is paused, everything previous has now been rendered to pause texture
	//so we set 'render' to false, causing everything else but the pause texture not being rendered in future frames
	ingame_menu.OnRender(renderer);

	//Debug information
	text_font.OnRender(0, 0, 0, 0, text_font.GetWidth(), text_font.GetHeight(), renderer);

	//Reset render target
	SDL_SetRenderTarget(renderer, nullptr);
	//Render to screen
	SDL_RenderPresent(renderer);
}

void GameMaster::SetRender(bool value)
{
	render = value;
}

