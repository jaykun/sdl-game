#pragma once
#ifndef CMAP_H_
#define CMAP_H_

#include <SDL.h>
#include <vector>

#include "GTile.h"
#include "GSurface.h"
#include "GTexture.h"

class GMap 
{
public:
	GMap();

	bool OnLoad(const char* File);

	void OnRender(SDL_Renderer *renderer, int MapX, int MapY);

	GTile* GetTile(int X, int Y);

	SDL_Surface* Surf_Tileset;
	GTexture* text;



private:
	std::vector<GTile> TileList;

};

#endif